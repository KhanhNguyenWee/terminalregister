package wee.digital.library.draw

import android.graphics.Color

data class PaintOptions(var color: Int = Color.BLACK, var strokeWidth: Float = 12f, var alpha: Int = 255, var isEraserOn: Boolean = false)
