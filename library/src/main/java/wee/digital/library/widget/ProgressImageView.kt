package wee.digital.library.widget

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.appcompat.widget.AppCompatImageView

class ProgressImageView : AppCompatImageView {

    private var animation: ObjectAnimator? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        startAnimation()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        stopAnimation()
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        if (visibility == View.VISIBLE) {
            startAnimation()
        } else {
            stopAnimation()
        }
    }

    private fun startAnimation() {
        if (animation == null) {
            animation = ObjectAnimator.ofFloat(this, "rotationY", 0.0f, 360f).also {
                it.duration = 1000
                it.interpolator = AccelerateDecelerateInterpolator()
                it.repeatCount = ObjectAnimator.INFINITE
            }
        }
        animation?.start()
    }

    private fun stopAnimation() {
        animation?.end()
    }

}