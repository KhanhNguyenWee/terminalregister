package wee.digital.library.adapter

import android.view.View

abstract class SelectableAdapter<T> : BaseRecyclerAdapter<T>() {

    var onItemSelected: (T, Int) -> Unit = { _, _ -> }

    open var selectedItemSelectable: T? = null

    final override var onItemClick: (T, Int) -> Unit = { model, index ->
        selectedItemSelectable = model
        notifyDataSetChanged()
        onItemSelected(model, index)
    }

    override fun View.onBindModel(model: T, position: Int, layout: Int) {
        onBindModel(model)
        if (isItemSelected(model)) {
            onBindModelSelected(model)
        } else {
            onBindModelUnselected(model)
        }
    }

    open fun isItemSelected(model: T): Boolean {
        return model == selectedItemSelectable
    }

    fun setDefaultList(list: List<T>) {
        set(list)
        list.firstOrNull()?.also {
            onItemClick(it, 0)
        }
    }

    protected abstract fun View.onBindModel(model: T)

    protected abstract fun View.onBindModelSelected(model: T)

    protected abstract fun View.onBindModelUnselected(model: T)

}