package wee.digital.library.adapter

import androidx.viewbinding.ViewBinding

abstract class SelectableBindAdapter<T> : BaseBindRecyclerAdapter<T>() {

    var onItemSelected: (T, Int) -> Unit = { _, _ -> }

    open var selectedItemSelectable: T? = null

    override fun ViewBinding.onBindItem(item: T, position: Int) {
        onBindModel(item)
        if (isItemSelected(item)) {
            onBindModelSelected(item)
        } else {
            onBindModelUnselected(item)
        }
    }

    override var onItemClick: (T, Int) -> Unit = { model, index ->
        selectedItemSelectable = model
        notifyDataSetChanged()
        onItemSelected(model, index)
    }

    open fun isItemSelected(model: T): Boolean {
        return model == selectedItemSelectable
    }

    fun setDefaultList(list: List<T>) {
        set(list)
        list.firstOrNull()?.also {
            onItemClick(it, 0)
        }
    }

    protected abstract fun ViewBinding.onBindModel(model: T)

    protected abstract fun ViewBinding.onBindModelSelected(model: T)

    protected abstract fun ViewBinding.onBindModelUnselected(model: T)

}