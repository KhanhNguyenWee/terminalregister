package wee.digital.library.util

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.net.Uri
import androidx.annotation.RawRes
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import wee.digital.library.Library.app
import wee.digital.library.extension.post
import java.io.File

object Media {

    val manager: AudioManager by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        (app.getSystemService(Context.AUDIO_SERVICE) as AudioManager).also {
            it.setStreamVolume(AudioManager.STREAM_MUSIC, 11, 0)
        }
    }

    private var mediaPlayer: MediaPlayer? = null

    private var isSilent: Boolean = false

    private var soundMap = mutableMapOf<Int, Int?>()

    private val soundPool: SoundPool by lazy {
        val attrs = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
        return@lazy SoundPool.Builder()
                .setMaxStreams(3)
                .setAudioAttributes(attrs)
                .build()
    }

    fun play(@RawRes raw: Int) {
        if (isSilent) return
        soundMap[raw]?.also {
            Single
                    .fromCallable {
                        soundPool.play(it, 1f, 1f, 1, 0, 1.0f)
                    }
                    .subscribeOn(Schedulers.newThread())
                    .subscribe()
            return
        }
        Single
                .fromCallable { soundPool.load(app, raw, 1) }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                .subscribe(object : SingleObserver<Int> {
                    override fun onSubscribe(d: Disposable?) {
                    }

                    override fun onSuccess(t: Int) {
                        soundMap[raw] = t
                        soundPool.play(t, 1f, 1f, 1, 0, 1.0f)
                    }

                    override fun onError(e: Throwable?) {
                    }

                })
    }

    fun release() {
        try {
            mediaPlayer?.release()
            mediaPlayer?.stop()
        } catch (e: IllegalStateException) {
        }
    }

    fun play(path: String) {
        try {
            mediaPlayer?.release()
        } catch (e: IllegalStateException) {
        }
        try {
            mediaPlayer = MediaPlayer.create(app, Uri.fromFile(File(path))).also {
                it.setOnCompletionListener {
                }
                it.setOnPreparedListener { player ->
                    player.start()
                }
            }
        } catch (ignore: Exception) {
        }
    }

}

