package wee.digital.library.util

import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference

class CenterSnapper {

    private val snapHelper = LinearSnapHelper()

    private var weakRecyclerView: WeakReference<RecyclerView?> = WeakReference(null)

    val currentPosition: Int
        get() {
            val recyclerView = weakRecyclerView.get() ?: return 0
            val view = snapHelper.findSnapView(recyclerView.layoutManager) ?: return 0
            return recyclerView.getChildAdapterPosition(view)
        }

    fun snap(view: RecyclerView, onSnap: ((Int) -> Unit)? = null) {
        weakRecyclerView = WeakReference(view)
        snapHelper.attachToRecyclerView(view)
        onSnap ?: return
        view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        val i = currentPosition
                        if (i != 0) {
                            onSnap(i)
                        }
                    }
                }
            }
        })
    }

}