package wee.digital.library.usb

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Parcelable
import wee.digital.library.usb.Usb

open class UsbDevicesReceiver(private val vendorIdList: IntArray) : BroadcastReceiver() {

    private var listener: UsbDevicesListener? = null

    val intentFilter: IntentFilter = IntentFilter(Usb.PERMISSION).also {
        it.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        it.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)
        it.addAction(UsbManager.EXTRA_PERMISSION_GRANTED)
    }

    private fun getUsbCreate() {
        for (vendorId in vendorIdList) {
            val usb = Usb.getDevice(vendorId)
            when {
                null == usb -> {
                }
                Usb.hasPermission(usb) -> {
                    listener?.onResult(usb, Usb.GRANTED)
                }
                else -> {
                    Usb.requestPermission(usb)
                }
            }
            usb?.let { listener?.onResult(it, Usb.GRANTED) }
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val usb = intent?.getParcelableExtra<Parcelable>(UsbManager.EXTRA_DEVICE) as? UsbDevice
            ?: return
        vendorIdList.forEach {
            if (it == usb.vendorId) {
                onUsbLiveDataUpdate(usb, intent)
                return
            }
        }
    }

    private fun Intent.getPermission(): Boolean {
        return getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)
    }

    private fun onUsbLiveDataUpdate(usb: UsbDevice, intent: Intent) {

        val status = when (intent.action) {

            UsbManager.ACTION_USB_DEVICE_DETACHED -> Usb.DETACHED

            UsbManager.ACTION_USB_DEVICE_ATTACHED -> if (Usb.hasPermission(usb)) {
                Usb.GRANTED
            } else {
                Usb.requestPermission(usb)
                Usb.ATTACHED
            }

            Usb.PERMISSION -> if (intent.getPermission()) {
                Usb.GRANTED
            } else {
                Usb.DENIED
            }

            else -> null

        } ?: return
        listener?.onResult(usb, status)
    }

    fun initListener(l: UsbDevicesListener) {
        listener = l
        getUsbCreate()
    }

    interface UsbDevicesListener {
        fun onResult(usb: UsbDevice, status: String)
    }

}