package wee.digital.library.usb

import android.hardware.usb.UsbDevice
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import wee.digital.library.util.EventLiveData

val usbLiveData: UsbLiveData by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
    UsbLiveData()
}

class UsbLiveData  : EventLiveData<UsbDevice>() {

    private var vendorIds: IntArray = intArrayOf()

    fun devices(vararg vendorIds: Int): UsbLiveData {
        this.vendorIds = vendorIds
        return this
    }

    fun observe(activity: AppCompatActivity, block: (UsbDevice) -> Unit) {
        for (vendorId in vendorIds) {
            val usb = Usb.getDevice(vendorId)
            when {
                null == usb -> {

                }
                Usb.hasPermission(usb) -> {
                    block(usb)
                }
                else -> {
                    Usb.requestPermission(usb)
                }
            }
            value = usb
        }
        observe(activity, Observer {
            if (null != it) {
                block(it)
            }
        })
        Usb.observer(activity, *vendorIds)
    }

}