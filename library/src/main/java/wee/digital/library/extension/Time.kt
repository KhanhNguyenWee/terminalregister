package wee.digital.library.extension

import java.lang.reflect.InvocationTargetException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val SECOND: Long = 1000

const val MIN: Long = 60 * SECOND

const val HOUR: Long = 60 * MIN

val nowInMillis: Long get() = System.currentTimeMillis()

val calendar: Calendar get() = Calendar.getInstance()

val nowInSecond: Long get() = System.currentTimeMillis() / SECOND

fun nowFormat(fmt: String): String {
    return nowInMillis.timeFormat(fmt)
}

fun nowFormat(sdf: SimpleDateFormat): String {
    return nowInMillis.timeFormat(sdf)
}

// if give up time in second convert to time in millis
private fun Long.correctTime(): Long {
    return if (this < 1000000000000L) this * 1000 else this
}

fun Long.timeFormat(fmt: SimpleDateFormat): String {
    return try {
        fmt.format(Date(this.correctTime()))
    } catch (e: ParseException) {
        ""
    } catch (e: InvocationTargetException) {
        ""
    }
}

fun Long.timeFormat(fmt: String): String {
    return timeFormat(SimpleDateFormat(fmt))
}

/**
 * [String] time convert
 */
fun String?.timeFormat(fmt: String): Long? {
    return timeFormat(SimpleDateFormat(fmt))
}

fun String?.timeFormat(fmt: SimpleDateFormat): Long? {
    this ?: return null
    return try {
        fmt.parse(this)?.time ?: 0
    } catch (e: ParseException) {
        null
    } catch (e: InvocationTargetException) {
        null
    }
}

fun String?.dateFormat(fmt: SimpleDateFormat): Date {
    this ?: return Date()
    return try {
        fmt.parse(this)
    } catch (e: ParseException) {
        return Date()
    } catch (e: InvocationTargetException) {
        return Date()
    }
}

/**
 * [Calendar] time convert
 */
fun Calendar.timeFormat(fmt: SimpleDateFormat): String {
    return try {
        fmt.format(this.time)
    } catch (e: ParseException) {
        ""
    } catch (e: InvocationTargetException) {
        ""
    }
}

fun Calendar.timeFormat(fmt: String): String {
    return timeFormat(SimpleDateFormat(fmt))
}

fun Calendar.isCurrentDay(momentCal: Calendar = calendar): Boolean {
    if (this.get(Calendar.YEAR) != momentCal.get(Calendar.YEAR)) return false
    if (this.get(Calendar.MONTH) + 1 != momentCal.get(Calendar.MONTH) + 1) return false
    return this.get(Calendar.DAY_OF_MONTH) == momentCal.get(Calendar.DAY_OF_MONTH)
}

fun Calendar.isYesterday(momentCal: Calendar = calendar): Boolean {
    if (this.get(Calendar.YEAR) != momentCal.get(Calendar.YEAR)) return false
    if (this.get(Calendar.MONTH) + 1 != momentCal.get(Calendar.MONTH) + 1) return false
    return this.get(Calendar.DAY_OF_MONTH) - momentCal.get(Calendar.DAY_OF_MONTH) == -1
}

fun Calendar.isTomorrow(momentCal: Calendar = calendar): Boolean {
    if (this.get(Calendar.YEAR) != momentCal.get(Calendar.YEAR)) return false
    if (this.get(Calendar.MONTH) != momentCal.get(Calendar.MONTH)) return false
    return this.get(Calendar.DAY_OF_MONTH) - momentCal.get(Calendar.DAY_OF_MONTH) == 1
}

fun String?.toValidDateFormat(fmt: SimpleDateFormat): String? {
    this ?: return null
    try {
        val date1 = fmt.parse(this)?:return null
        return fmt.format(date1)
    } catch (ignore: Exception) {
    }
    return null
}

fun String?.toValidDateFormat(fmt: String): String? {
    return toValidDateFormat(SimpleDateFormat(fmt))
}




