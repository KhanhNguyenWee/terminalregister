package wee.digital.library.extension

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.SystemClock
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

abstract class ViewClickListener : View.OnClickListener {

    private var lastClickTime: Long = 0

    private var lastClickViewId: Int = -1

    abstract fun onClicks(v: View?)

    final override fun onClick(v: View?) {
        if (SystemClock.elapsedRealtime() - lastClickTime > 500 || v?.id != lastClickViewId) {
            lastClickViewId = v?.id ?: -1
            onClicks(v)
        }
        lastClickTime = SystemClock.elapsedRealtime()
    }

}

fun View.addViewClickListener(block: (View?) -> Unit) {
    setOnClickListener(object : ViewClickListener() {
        override fun onClicks(v: View?) {
            block(v)
        }
    })
}

fun View?.addViewClickListener(listener: View.OnClickListener?) {
    this ?: return
    if (this is EditText && listener != null) {
        isFocusable = false
        isCursorVisible = false
        keyListener = null
        inputType = EditorInfo.IME_ACTION_NONE
    }
    setOnClickListener(listener)
}

abstract class FastClickListener(private val clickCount: Int) : View.OnClickListener {

    private var lastClickTime: Long = 0

    private var currentClickCount: Int = 0

    abstract fun onViewClick(v: View?)

    final override fun onClick(v: View?) {
        if (System.currentTimeMillis() - lastClickTime > 500 || currentClickCount >= clickCount) {
            currentClickCount = 0
        }
        lastClickTime = System.currentTimeMillis()
        currentClickCount++
        if (currentClickCount == clickCount) {
            lastClickTime = 0
            currentClickCount = 0
            onViewClick(v)
        }
    }

}

fun View.addFastClickListener(clickCount: Int, block: () -> Unit) {
    setOnClickListener(object : FastClickListener(clickCount) {
        override fun onViewClick(v: View?) {
            block()
        }
    })
}

fun MotionEvent.onActionHideKeyboard(activity: Activity){
    if (this.action == MotionEvent.ACTION_DOWN) {
        activity.hideSystemUI()
        val v = activity.currentFocus
        if (v is EditText) {
            val outRect = Rect()
            v.getGlobalVisibleRect(outRect)
            if (!outRect.contains(this.rawX.toInt(), this.rawY.toInt())) {
                v.clearFocus()
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
            }
        }
    }
}