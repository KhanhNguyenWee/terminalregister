package wee.digital.library.extension

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream
import java.math.BigDecimal

fun Bitmap?.encodeToString(): String {
    return toBytes()?.encodeToString()
}

fun Bitmap?.toBytes(): ByteArray {
    this ?: return ByteArray(1)
    return try {
        val stream = ByteArrayOutputStream()
        this.copy(Bitmap.Config.RGB_565, true)?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val byteArray = stream.toByteArray()
        stream.close()
        byteArray
    } catch (e: Exception) {
        ByteArray(1)
    }
}

fun ByteArray?.encodeToString(): String {
    return Base64.encodeToString(this, Base64.NO_WRAP)
}

fun String?.decodeToBytes(): ByteArray? {
    return Base64.decode(this, Base64.NO_WRAP)
}

fun BigDecimal?.isNullOrZero(): Boolean {
    return this == null || this == BigDecimal.ZERO
}

fun ByteArray?.decodeToBitmap(): Bitmap? {
    this ?: return null
    return BitmapFactory.decodeByteArray(this, 0, size)
}

