function Effect() {
    var self = this;
    
    this.faceActions = [];
    this.noFaceActions = [];
    this.videoRecordStartActions = [];
    this.videoRecordFinishActions = [];
    this.videoRecordDiscardActions = [];
    
    this.init = function() {
        Api.showRecordButton();
        Api.meshfxMsg("spawn", 3, 0, "!glfx_FACE");
        Api.playVideo("frx", true, 1);
        Api.meshfxMsg("spawn", 2, 0, "FaceDefault.bsm2");
    };
}

// R G B A -- color
// R = 18 / 255 = 0.0706
// G = 121 / 255 = 0.4745
// B = 218 / 255 = 0.8549
// A - intensity multiplier
var color =  "0.0706 0.4745 0.8549 1.0";


configure(new Effect());
