function Effect() {
    var self = this;
    
    this.faceActions = [];
    this.noFaceActions = [];
    this.videoRecordStartActions = [];
    this.videoRecordFinishActions = [];
    this.videoRecordDiscardActions = [];
    
    this.init = function() {
        Api.showRecordButton();
        Api.meshfxMsg("spawn", 2, 0, "!glfx_FACE");
        
        Api.playVideo("frx", false, 1);
    };
}

configure(new Effect());
