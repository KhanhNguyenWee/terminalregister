#version 300 es

precision highp float;

layout(std140) uniform glfx_GLOBAL
{
    mat4 glfx_MVP;
    mat4 glfx_PROJ;
    mat4 glfx_MV;
    vec4 glfx_QUAT;
    vec4 js_color;
};

in vec2 var_uv;

layout( location = 0 ) out vec4 frag_color;

uniform sampler2D glfx_VIDEO;

void main()
{
	vec2 uv = var_uv;
	frag_color = js_color * texture(glfx_VIDEO,uv) * js_color.w;
}

