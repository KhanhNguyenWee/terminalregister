package wee.digital.terminalregister.repository.model

data class SocketResponse(
    var command: String = "",
    var content: SocketBody = SocketBody()
)

data class SocketBody(
    var memberID: String = "",
    var receptionID: String = ""
)