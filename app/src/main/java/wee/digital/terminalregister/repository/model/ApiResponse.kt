package wee.digital.terminalregister.repository.model

import org.json.JSONObject

class ApiResponse {
    var code: Int = -1

    var message: String = ""

    var data: JSONObject? = null
}