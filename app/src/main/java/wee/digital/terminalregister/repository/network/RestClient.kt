package wee.digital.terminalregister.repository.network

import android.util.Log
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import wee.digital.terminalregister.shared.Key
import wee.digital.terminalregister.shared.NetworkUrl
import java.util.concurrent.TimeUnit

object RestClient {

    val rest: ApiService by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        getClient().create(ApiService::class.java)
    }

    private const val timeOut = 10L

    fun getClient(): Retrofit {
        val client = initOkHttp()
        val retrofit = Retrofit.Builder()
            .baseUrl(NetworkUrl.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit as Retrofit
    }

    private fun initOkHttp(): OkHttpClient {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer " + Key.AUTH_TOKEN)
            val request = requestBuilder.build()
            Log.d("requestBuilder","${request.headers}")
            chain.proceed(request)
        }
        return httpClient.build()
    }

}