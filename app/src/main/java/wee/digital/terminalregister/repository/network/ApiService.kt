package wee.digital.terminalregister.repository.network

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url

@JvmSuppressWildcards
interface ApiService {

    @POST
    suspend fun postApi(
        @Url url: String,
        @Body body: RequestBody
    ): Response<ResponseBody>

}