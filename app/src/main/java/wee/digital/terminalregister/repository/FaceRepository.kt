package wee.digital.terminalregister.repository

import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import wee.digital.terminalregister.repository.model.ApiResponse
import wee.digital.terminalregister.repository.model.FaceRegisterReq
import wee.digital.terminalregister.repository.model.UpdateFaceReq
import wee.digital.terminalregister.repository.network.RestClient
import wee.digital.terminalregister.repository.network.handleResp
import wee.digital.terminalregister.repository.network.handlerApi

object FaceRepository {

    fun registerFace(data: FaceRegisterReq) = flow<ApiResponse> {
        try {
            val json = Gson().toJson(data)
            val request = json.toRequestBody("application/octet-stream".toMediaTypeOrNull())
            val api =
                RestClient.rest.postApi("facepay/enrollNewUser", request).handleResp()
            emit(api)
        } catch (e: java.lang.Exception) {
            emit(e.handlerApi())
        }
    }.flowOn(Dispatchers.IO)

}