package wee.digital.terminalregister.repository.model

import com.google.gson.annotations.SerializedName

data class UpdateFaceReq(
    @SerializedName("deviceID")
    var deviceID: String = "",

    @SerializedName("memberCode")
    var memberCode: String = "",

    @SerializedName("clubID")
    var clubID: String = "",

    @SerializedName("faceData")
    var face: String = "",

    @SerializedName("avatarData")
    var avatar: String = ""
)

data class UpdateFaceResp(
    @SerializedName("code")
    var code: Int = -1,

    @SerializedName("message")
    var message: String = "",

    @SerializedName("result")
    var result: UpdateFaceBody = UpdateFaceBody()
)

data class UpdateFaceBody(
    @SerializedName("faceID")
    var faceID: String = "",
)


data class RemoveFaceReq(
    @SerializedName("faceData")
    var face: String = ""
)

data class RemoveFaceResp(
    @SerializedName("code")
    var code: Int = -1,

    @SerializedName("message")
    var message: String = ""
)