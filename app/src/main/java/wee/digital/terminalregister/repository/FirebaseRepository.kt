package wee.digital.terminalregister.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flowOn
import wee.digital.terminalregister.repository.model.EffectFirebaseData
import wee.digital.library.extension.parse
import wee.digital.library.extension.toJsonObject
import java.util.*

object FirebaseRepository {

    private val database = Firebase.database

    private val databaseEffect = database.getReference("effect")

    private var listenerEffect: ValueEventListener? = null

    fun eventEffectFirebase() = callbackFlow<ArrayList<EffectFirebaseData>> {
        listenerEffect = databaseEffect.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                try {
                    val effects = snapshot.parseEffects()
                    offer(effects)
                } catch (e: Exception) {
                    close(e)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                close(java.lang.Exception(error.message))
            }
        })
        awaitClose { cancel() }
    }

    /**
     * effect firebase
     */
    fun DataSnapshot.parseEffects(): ArrayList<EffectFirebaseData> {
        val effectList = arrayListOf<EffectFirebaseData>()
        (this.value as HashMap<*, *>).forEach {
            val card = it.value.toJsonObject().parse(EffectFirebaseData::class.java)
                ?: EffectFirebaseData()
            effectList.add(card)
        }
        effectList.sortedWith(compareBy({ it.isUse }, { it.isUse }))
        return effectList
    }

}