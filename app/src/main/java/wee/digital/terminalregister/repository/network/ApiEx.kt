package wee.digital.terminalregister.repository.network

import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import wee.digital.terminalregister.repository.model.ApiResponse
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun Response<ResponseBody>.handleResp(): ApiResponse {
    val resp = this@handleResp
    val result = ApiResponse()
    if (resp.code() != 200) {
        result.apply {
            code = resp.code()
            message = resp.message()
        }
        return result
    }
    return try {
        val json = JSONObject(resp.body()?.string() ?: "")
        result.apply {
            code = json.getInt("code")
            message = json.getString("message") ?: ""
            data = json
        }
        result
    } catch (e: Exception) {
        result.apply {
            code = -1
            message = "convert data fail"
        }
        result
    }
}

fun java.lang.Exception.handlerApi(): ApiResponse {
    return when (this) {
        is HttpException, is UnknownHostException, is SocketTimeoutException -> {
            val api = ApiResponse().apply {
                code = -1
                message = this@handlerApi.message.toString()
            }
            api
        }
        else -> {
            val api = ApiResponse().apply {
                code = -100
                message = this@handlerApi.message.toString()
            }
            api
        }
    }
}