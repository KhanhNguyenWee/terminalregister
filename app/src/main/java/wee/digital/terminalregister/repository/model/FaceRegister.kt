package wee.digital.terminalregister.repository.model

import com.google.gson.annotations.SerializedName

data class FaceRegisterReq(
    @SerializedName("cif")
    var cif: String = "",

    @SerializedName("photo")
    var photo: String = ""
)

data class FaceRegisterResponse (
    @SerializedName("code")
    var code: Int = -1,
    @SerializedName("message")
    var message: String = "",
    @SerializedName("metric")
    var metric: Float = 0.0F,
    @SerializedName("duplicated")
    var duplicatedData: DataDuplicated ?= null,
)

data class DataDuplicated(
    @SerializedName("isIdentical")
    var isIdentical: Boolean = false,
    @SerializedName("isSame")
    var isSame: Boolean = false,
    @SerializedName("score")
    var score: Int = 0,
    @SerializedName("confidence")
    var confidence: Int = 0,
)
