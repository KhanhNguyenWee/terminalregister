package wee.digital.terminalregister.repository.model


class AdvItem(
    val imageRes: Int? = null
) {

    val isImage get() = imageRes != null

}