package wee.digital.citigym_register.repository.socket

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.WebSocketListener
import java.util.concurrent.TimeUnit

class WebSocketControl : WebSocketListener() {

    var listener: WebSocketListener? = null

    private var socket: okhttp3.WebSocket? = null

    var isConnected = false

    fun connectUrl(url: String) {
        if (isConnected) return
        isConnected = true
        val client = OkHttpClient.Builder()
            .readTimeout(3, TimeUnit.SECONDS)
            .build()
        val request = Request.Builder()
            .url(url)
            .build()
        socket = client.newWebSocket(request, this)
    }

    override fun onOpen(webSocket: okhttp3.WebSocket, response: Response) {
        super.onOpen(webSocket, response)
        listener?.connected()
    }

    override fun onClosed(webSocket: okhttp3.WebSocket, code: Int, reason: String) {
        super.onClosed(webSocket, code, reason)
        isConnected = false
        listener?.disconnected()
    }

    override fun onFailure(webSocket: okhttp3.WebSocket, t: Throwable, response: Response?) {
        super.onFailure(webSocket, t, response)
        isConnected = false
        listener?.disconnected()
    }

    override fun onMessage(webSocket: okhttp3.WebSocket, text: String) {
        super.onMessage(webSocket, text)
        listener?.message(text)
    }

    fun sendData(text: String) {
        socket?.send(text)
    }

    fun disconnectUrl() {
        socket?.close(1000, "close socket")
        isConnected = false
    }

    interface WebSocketListener {
        fun connected() {}
        fun disconnected() {}
        fun message(str: String) {}
    }

}