package wee.digital.terminalregister.repository

import android.graphics.Point
import android.graphics.Rect
import wee.digital.terminalregister.camera.face.RealSenseControl

data class DataCollect(
    var unit: Float = 0f,
    var depthData: ByteArray? = null,
    var colorData: ByteArray? = null,
    val width: Int = RealSenseControl.COLOR_WIDTH,
    val height: Int = RealSenseControl.COLOR_HEIGHT,
    var device: RealSenseControl.DeviceConfig = RealSenseControl.DeviceConfig(),
    var pointData: String = "",
    var dataFacePoint: DataGetFacePoint? = null,
    var isRepaired: Boolean = false,
    var frameColorString: String = "",
    var frameDepthString: String = ""
)

data class FacePointData(
        var faceRect: Rect,
        var RightEye: Point,
        var LeftEye: Point,
        var Nose: Point,
        var Rightmouth: Point,
        var Leftmouth: Point
)

data class DataGetFacePoint(
    val dataFace: FacePointData?,
    val face: ByteArray?,
    var fullFrame: ByteArray? = null
)