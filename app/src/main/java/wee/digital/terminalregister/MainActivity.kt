package wee.digital.terminalregister

import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.intel.realsense.librealsense.RsContext
import com.intel.realsense.librealsense.UsbUtilities
import wee.digital.terminalregister.app.App
import wee.digital.terminalregister.app.app
import wee.digital.terminalregister.camera.face.RealSenseControl
import wee.digital.terminalregister.databinding.ActivityMainBinding
import wee.digital.terminalregister.ui.base.BaseActivity
import wee.digital.terminalregister.ui.base.activityVM
import wee.digital.terminalregister.ui.main.Main
import wee.digital.terminalregister.utils.ex.connectCamera

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val sharedVM by lazy { activityVM(SharedVM::class) }

    override fun viewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onViewCreated() {
        FirebaseApp.initializeApp(this)
        RsContext.init(app)
        UsbUtilities.grantUsbPermissionIfNeeded(app)
        App.realSenseControl = RealSenseControl()
        connectCamera()
    }

    override fun onLiveDataObserve() {
        sharedVM.message.observe {
            it ?: return@observe
            navigate(Main.message)
        }
    }

    override fun navigationHostId(): Int {
        return R.id.mainContainer
    }
}