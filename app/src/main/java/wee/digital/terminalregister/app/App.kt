package wee.digital.terminalregister.app

import android.app.Application
import android.widget.Toast
import com.banuba.sdk.manager.BanubaSdkManager
import wee.digital.library.Library
import wee.digital.library.extension.mainThread
import wee.digital.terminalregister.camera.face.BanubaImp
import wee.digital.terminalregister.camera.face.RealSenseControl
import wee.digital.terminalregister.utils.OpenCVUtils
import wee.digital.terminalregister.utils.ex.stopCamera

lateinit var app: App private set

class App : Application() {
    var banubaImp: BanubaImp? = null

    companion object {
        var realSenseControl: RealSenseControl? = null

    }

    override fun onCreate() {
        super.onCreate()
        app = this
        app.onModulesInject()
        initBanuba()
        OpenCVUtils.initOpenCV(this)
    }

    override fun onTerminate() {
        stopCamera()
        super.onTerminate()
    }

    private fun onModulesInject() {
        Library.app = this
    }

    private fun initBanuba() {
        BanubaSdkManager.initialize(this.applicationContext, BanubaImp.tokenBanuba)
        banubaImp = BanubaImp(this).apply {
            checkEffect { unApplyEffect() }
        }
    }
}

private var curToast: Toast? = null
fun toast(message: String?) {
    message ?: return
    mainThread {
        curToast?.cancel()
        curToast = Toast.makeText(app, message, Toast.LENGTH_SHORT)
        curToast?.show()
    }
}