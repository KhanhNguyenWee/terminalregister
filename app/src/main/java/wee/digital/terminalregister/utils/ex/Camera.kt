package wee.digital.terminalregister.utils.ex

import androidx.fragment.app.Fragment
import wee.digital.library.extension.SimpleLifecycleObserver
import wee.digital.terminalregister.app.App
import wee.digital.terminalregister.camera.face.RealSenseControl


fun Fragment.observerCameraListener(listener: RealSenseControl.Listener) {
    viewLifecycleOwner.lifecycle.addObserver(object : SimpleLifecycleObserver() {
        override fun onResume() {
            super.onResume()
            App.realSenseControl?.listener = listener
        }

        override fun onPause() {
            App.realSenseControl?.listener = null
        }

    })
}

fun Fragment.observerCameraListenerQr(listener: RealSenseControl.Listener) {
    viewLifecycleOwner.lifecycle.addObserver(object : SimpleLifecycleObserver() {
        override fun onResume() {
            super.onResume()
            App.realSenseControl?.listener = listener
        }

        override fun onPause() {
            App.realSenseControl?.listener = null
        }

    })
}

fun connectCamera() {
    App.realSenseControl?.startStreamThread()
}

fun startCamera() {
    App.realSenseControl?.onResume()
}

fun pauseCamera() {
    App.realSenseControl?.onPause()
}

fun stopCamera() {
    App.realSenseControl?.stopStreamThread()
}
