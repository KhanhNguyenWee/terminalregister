package wee.digital.terminalregister.utils

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.navigation.fragment.findNavController
import androidx.transition.ChangeBounds
import wee.digital.library.extension.beginTransition
import wee.digital.library.extension.color
import wee.digital.library.extension.post

val viewTransition = ChangeBounds().apply { duration = 300 }

val viewNonAnim = ChangeBounds().apply { duration = 0 }

/*
facepay magic
private fun animBanner() {
    val colorFrom = color(R.color.colorBlue)
    val colorTo = color(R.color.colorWhite)
    val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
    colorAnimation.duration = 300

    colorAnimation.addUpdateListener { animator -> bd.paymentView.setBackgroundColor(animator.animatedValue as Int) }
    colorAnimation.start()

    val valueAlpha = ValueAnimator.ofFloat(1f, 0f).apply {
        interpolator = LinearInterpolator()
        duration = 300
        addUpdateListener {
            val value = it.animatedValue as Float
            bd.paymentAction.alpha = value
            bd.paymentRootNote.alpha = value
            bd.paymentRecyclerView.alpha = value
        }
    }
    valueAlpha.start()

    viewTransition.beginTransition(bd.paymentView) {
        this.clear(bd.paymentRootBanner.id, ConstraintSet.TOP)
        this.clear(bd.paymentRootBanner.id, ConstraintSet.BOTTOM)
        connect(
            bd.paymentRootBanner.id,
            ConstraintSet.TOP,
            bd.paymentGuideline3.id,
            ConstraintSet.TOP
        )
    }
    post(300) { findNavController().navigate(NavigationId.face) }
}*/
