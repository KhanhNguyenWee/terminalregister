package wee.digital.terminalregister.utils

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import wee.digital.terminalregister.app.app
import java.io.File
import kotlin.concurrent.thread

@Suppress("DEPRECATION")
class MyDownloadManager(val context: Context) {

    companion object {
        const val TIMEOUT = 20000L
    }

    private var downloadManager: DownloadManager? = null

    private var downloadImageId: Long = -1

    private var handler = Handler(Looper.getMainLooper())

    private var runnable: Runnable = Runnable { }

    private var nameEffect = ""

    private var curStatus = 0f

    private var isDownloading = false

    private var mListener: MyDownloadListener? = null

    private var fileDownloaded: File? = null

    val externalDir: File
        get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val file = File(app.getExternalFilesDir(null)!!, "Download/effects")
            if (!file.exists()) {
                file.mkdirs()
            }
            file
        } else {
            @Suppress("DEPRECATION")
            val file = File(Environment.getExternalStorageDirectory(), "Download/effects")
            if (!file.exists()) {
                file.mkdirs()
            }
            file
        }

    fun setListener(listener: MyDownloadListener) {
        mListener = listener
    }

    fun start(title: String, url: String, name: String) {
        nameEffect = name
        if (downloadManager == null) downloadManager =
            context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadImageId = startDownload(url, nameEffect)
        context.registerReceiver(
            onComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        mListener?.onStart()
        Toast.makeText(context, "$nameEffect Downloading...", Toast.LENGTH_SHORT).show()
    }


    private fun startDownload(url: String, nameEffect: String): Long {
        val downloadReference: Long
        val request = DownloadManager.Request(Uri.parse(url))
        request.setTitle("Download Camera Effects")
        request.setDescription("Effect $nameEffect apply for camera")
        fileDownloaded = File(externalDir, "${nameEffect}.zip")
        fileDownloaded?.deleteOnExit()
        request.setDestinationUri(Uri.fromFile(fileDownloaded))
        downloadReference = downloadManager?.enqueue(request) ?: -1
        Log.e("DownloadEffect", "Start $downloadReference ${fileDownloaded?.path}")
        return downloadReference
    }

    private fun getDownloadState(downloadID: Long): Float {
        val query = DownloadManager.Query().setFilterById(downloadID)
        val c = downloadManager?.query(query)
        return try {
            if (c != null) {
                if (c.moveToFirst()) {
                    val status = c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_STATUS))
                    return when (status) {
                        DownloadManager.STATUS_PENDING -> 0f
                        DownloadManager.STATUS_PAUSED, DownloadManager.STATUS_RUNNING -> {
                            val download =
                                c.getFloat(c.getColumnIndexOrThrow(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                            val all =
                                c.getFloat(c.getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
                            download / all
                        }
                        DownloadManager.STATUS_SUCCESSFUL -> 100f
                        DownloadManager.STATUS_FAILED -> -1f
                        else -> -1f
                    }
                }
                -1f
            } else {
                -1f
            }
        } catch (e: Exception) {
            e.printStackTrace()
            -1f
        } finally {
            c?.close()
        }
    }

    private var onComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                DownloadManager.ACTION_DOWNLOAD_COMPLETE -> {
                    Log.e("DownloadEffect", "Done ${fileDownloaded?.path}")
                    fileDownloaded ?: return
                    if (fileDownloaded!!.exists()) {
                        thread(start = true) {
                            //unzip(fileDownloaded!!, getEffectsDirectory(context))
                            downloadManager?.remove(downloadImageId)
                            context.unregisterReceiver(this)
                            mListener?.onDone()
                        }
                    }
                }
            }
        }
    }

    fun stopDownload() {
        handler.removeCallbacks(runnable)
    }

    interface MyDownloadListener {
        fun onStart()
        fun onDone()
        fun onFailed()
    }

}