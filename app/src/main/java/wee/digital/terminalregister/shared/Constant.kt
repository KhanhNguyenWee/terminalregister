package wee.digital.terminalregister.shared

import java.text.SimpleDateFormat
import java.util.*

object Configs {
    const val DEFAULT_ARG_KEY: String = "default"

    val DEFAULT_DATE_FMT = SimpleDateFormat("dd/MM/yyyy")

    val DEFAULT_DATE_TIME_FMT = SimpleDateFormat("HH:mm dd/MM/yyyy")

    const val FACE_RETRY_COUNT = 5

    const val PIN_RETRY_COUNT = 3

    var TESTING = false

    val DEVICE_NAME_FILTER = charArrayOf(
        'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',
        'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' '
    )
}



object Key {
    const val AUTH_TOKEN = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1dWlkIjoiNjE4Y2JhZTU5ZDgwMjhlYzJjNjg1ZWE5IiwiZXhwIjoxNjcwOTMzNjQwLCJqdGkiOiI2MWI3MzkwOGNhNmJhM2ZkMTQ5ZjFlNjAiLCJpYXQiOjE2MzkzOTc2NDB9.u6Y58v32Iz70SfGOYA_bHyNGn0mOxCtqtPsle5KUJsl3NrPYa3u_3tWs5S__V-Kjm9zpjZemZ4uInNMkgjWArQ"
}

object NetworkUrl{
    const val BASE_URL = "https://dev.facepay.vn/vncard/"
}

object Code {
    const val CIF_INVALID = 1101
    const val CIF_DUPLICATE = 1102
    const val PHOTO_BLANK = 1104
    const val PHOTO_INVALID = 1105
    const val MORE_THAN_ONE_FACE = 1106
    const val PHOTO_NOT_MEET_CRITERIA = 1107
    const val BIO_ALREADY_EXIST = 19
    const val SERVICE_UNAVAILABLE = 53
}