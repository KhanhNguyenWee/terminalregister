package wee.digital.terminalregister.shared

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import wee.digital.library.util.EventLiveData
import wee.digital.terminalregister.R
import java.util.*

object Shared {

    val imageLiveData = EventLiveData<Bitmap?>()

    fun getUUIDRandom(): String {
        return UUID.randomUUID().toString()
    }
}