package wee.digital.terminalregister.message

import wee.digital.library.extension.string
import wee.digital.terminalregister.R
import wee.digital.terminalregister.shared.Code

open class MessageArg {

    var icon: Int = R.drawable.ic_fail
    var title: String? = null
    var timeout: Long = 5000L
    var message: String? = null
    var buttonClose: String? = null
    var buttonHorizontal1: String? = null
    var buttonHorizontal2: String? = null
    var onClose: (MessageFragment) -> Unit = {}
    var onButtonHorizontal1: (MessageFragment) -> Unit = {}
    var onButtonHorizontal2: (MessageFragment) -> Unit = {}

    companion object {

        val alreadyCheckIn = MessageArg().apply {
            title = string(R.string.already_check_in_title)
            message = string(R.string.already_check_in_text)
            buttonHorizontal2= string(R.string.message_action_accept)
        }

        val errorRegister = MessageArg().apply {
            title = string(R.string.fail_title)
            message = string(R.string.fail_text)
        }


        fun fromCode(code: Int): MessageArg {
            return when (code) {
                Code.BIO_ALREADY_EXIST -> alreadyCheckIn
                else -> errorRegister
            }
        }

    }
}