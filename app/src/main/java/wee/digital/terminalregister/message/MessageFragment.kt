package wee.digital.terminalregister.message

import android.view.View
import wee.digital.library.extension.hide
import wee.digital.library.extension.setHyperText
import wee.digital.library.extension.show
import wee.digital.library.extension.string
import wee.digital.terminalregister.MainDialog
import wee.digital.terminalregister.R
import wee.digital.terminalregister.databinding.MessageBinding

class MessageFragment : MainDialog<MessageBinding>(MessageBinding::inflate) {

    private val messageParButton get() = bd.messageButtonParent

    override fun onViewCreated() {
        dialog?.window?.attributes?.windowAnimations = R.style.App_DialogAnimScale
        addClickListener(
            bd.messageViewClose,
            messageParButton.messageButton1,
            messageParButton.messageButton2
        )
    }

    override fun onLiveDataObserve() {
        sharedVM.message.observe {
            if (it == null) {
                dismissAllowingStateLoss()
                return@observe
            }
            onBindArg(it)
        }
        sharedVM.dismissDialog.observe {
            dismissAllowingStateLoss()
        }
    }

    override fun onViewClick(v: View?) {
        when (v) {
            bd.messageViewClose -> {
                dismiss()
                sharedVM.message.value?.onClose?.also { it(this) }
            }
            messageParButton.messageButton1 -> {
                sharedVM.message.value?.onButtonHorizontal1?.also { it(this) }
            }
            messageParButton.messageButton2 -> {
                sharedVM.message.value?.onButtonHorizontal2?.also { it(this) }
            }
        }
    }

    private fun onBindArg(arg: MessageArg) {
        bd.messageImageViewIcon.setImageResource(arg.icon)
        bd.messageTextViewTitle.text = arg.title ?: string(R.string.app_name)
        bd.messageTextViewMessage.setHyperText(arg.message)
        if (arg.buttonClose.isNullOrEmpty()) {
            bd.messageViewClose.hide()
            bd.messageRootAction.show()
            messageParButton.messageButton1.text = arg.buttonHorizontal1
            messageParButton.messageButton2.text = arg.buttonHorizontal2
        } else {
            bd.messageViewClose.show()
            bd.messageViewClose.text = arg.buttonClose
        }
    }

}