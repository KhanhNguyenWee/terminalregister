package wee.digital.terminalregister.camera

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.ImageFormat
import android.graphics.PixelFormat
import android.os.Build
import android.os.Environment
import android.util.Log
import wee.digital.terminalregister.camera.face.BanubaImp
import wee.digital.library.extension.mainThread
import wee.digital.library.util.Logger
import wee.digital.terminalregister.app.app
import wee.digital.terminalregister.repository.model.EffectFirebaseData
import wee.digital.terminalregister.utils.MyDownloadManager
import java.io.*
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.charset.Charset
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

private val log = Logger("BanubaEx")

val filePath: String
    get() {
        if (Build.VERSION.SDK_INT > -Build.VERSION_CODES.Q) {
            return app.getExternalFilesDir(null)?.absolutePath!!
        } else {
            @Suppress("DEPRECATION")
            return Environment.getExternalStorageDirectory().toString()
        }
    }

fun checkEffectDownloaded(context: Context, effectName: String): Boolean {
    val path = "${context.dataDir}/files/banuba/bnb-resources/effects/$effectName"
    val directory = File(path)
    return directory.isDirectory
}

fun checkEffectAsset(context: Context, block: () -> Unit) {
    val files = context.assets.list("bnb-resources/effects")
    files?.forEach { effectName ->
        log.d("checkEffectAsset : Checking... $effectName")
        if (!checkEffectDownloaded(context, effectName)) {
            val timeIn = System.currentTimeMillis()
            val path = "${context.dataDir}/files/banuba/bnb-resources/effects/$effectName"
            val fileCache = File(path)
            fileCache.mkdirs()
            copyAssetFolder("bnb-resources/effects/$effectName", fileCache.path)
            log.d("checkEffectAsset : Copy... $effectName [${System.currentTimeMillis() - timeIn}]")
        }
    }
    block()
}

fun copyAssetFolder(assetPath: String, destinationPath: String = filePath) {
    return try {
        val files = app.assets.list(assetPath)?.iterator() ?: return
        File(destinationPath).mkdirs()
        for (file in files) {
            if (file.contains(".")) {
                createFile("$assetPath/$file", "$destinationPath/$file")
            } else {
                log.d("copyAssetFolder")
                copyAssetFolder("$assetPath/$file", "$destinationPath/$file")
            }
        }
        log.d("copyAssetFolder completed")
    } catch (e: java.lang.Exception) {
        println(e.message)
    }
}

fun copyFile(assetPath: String, fileName: String) {
    var inputStream: InputStream? = null
    var fos: FileOutputStream? = null
    try {
        inputStream = app.assets.open(assetPath)
        fos = FileOutputStream(fileName)
        val buffer = ByteArray(1024)
        var read: Int = inputStream.read(buffer)
        while (read >= 0) {
            fos.write(buffer, 0, read)
            read = inputStream.read(buffer)
        }
    } catch (e: IOException) {
        log.e("copyFile : ${e.message}")
    }
    inputStream?.close()
    fos?.flush()
    fos?.close()
}

fun createFile(assetPath: String, fileName: String) {
    try {
        val file = File(fileName)
        if (file.exists()) {
            file.delete()
        }
        file.createNewFile()
        copyFile(assetPath, fileName)
    } catch (e: IOException) {
        log.e("createFile : ${e.message}")
    }
}

fun getEffectsDirectory(context: Context): File {
    val path = "${context.dataDir}/files/banuba/bnb-resources/effects"
    return File(path)
}

fun checkEffectFromFirebase(
    context: Context,
    listEffect: ArrayList<EffectFirebaseData>,
    block: () -> Unit
) {
    val listDownload = ArrayList<EffectFirebaseData>()
    listEffect.forEach { effect ->
        if (!checkEffectDownloaded(context, effect.name)) {
            listDownload.add(effect)
        }
    }
    if (listDownload.isNotEmpty()) {
        val dlManager = MyDownloadManager(context)
        dlManager.setListener(object : MyDownloadManager.MyDownloadListener {
            override fun onStart() {

            }

            override fun onDone() {
                mainThread {
                    if (listDownload.isNotEmpty()) {
                        val effFirst = listDownload.first()
                        listDownload.remove(effFirst)
                        dlManager.start("Download Effects", effFirst.url, effFirst.name)
                    } else {
                        block()
                    }
                }

            }

            override fun onFailed() {
                mainThread {
                    if (listDownload.isNotEmpty()) {
                        val effFirst = listDownload.first()
                        listDownload.remove(effFirst)
                        dlManager.start("Download Effects", effFirst.url, effFirst.name)
                    } else {
                        block()
                    }
                }

            }

        })
        val effFirst = listDownload.first()
        listDownload.remove(effFirst)
        dlManager.start("Download Effects", effFirst.url, effFirst.name)

    }else{
        block()
    }
}

@Throws(IOException::class)
fun unzip(zipFile: File?, targetDirectory: File) {
    zipFile ?: return
    Log.e("DownloadEffect","Start Unzip: ${zipFile.path}")
    ZipInputStream(BufferedInputStream(FileInputStream(zipFile))).use { zipInputStream ->
        var ze: ZipEntry? = null
        var count: Int
        val buffer = ByteArray(8192)
        while (zipInputStream.nextEntry.also { ze = it } != null) {
            val zipEntry = ze ?: break
            val file = File(targetDirectory, zipEntry.name)
            val dir = if (zipEntry.isDirectory) file else file.parentFile
            if (!dir.isDirectory && !dir.mkdirs()) throw FileNotFoundException(
                "Failed to ensure directory: " +
                        dir.absolutePath
            )
            if (zipEntry.isDirectory) continue
            val fileOutputStream = FileOutputStream(file)
            fileOutputStream.use { fileOut ->
                while (zipInputStream.read(buffer).also { count = it } != -1) fileOut.write(
                    buffer,
                    0,
                    count
                )
            }
            /* if time should be restored as well
            long time = ze.getTime();
            if (time > 0)
                file.setLastModified(time);
            */
        }
        Log.e("DownloadEffect","Done Unzip: ${zipFile.path}")
        zipFile.delete()
    }
}

fun zipFile(inputFile: File, outputZipFile: File) {
    val reader = BufferedReader(FileReader(inputFile))
    val zipStream = ZipOutputStream(FileOutputStream(outputZipFile))
    val writer = BufferedWriter(
        OutputStreamWriter(zipStream, Charset.forName("ISO-8859-1"))
    )
    // this is the important part:
    // all character data is written via the writer and not the zip output stream

    var line: String? = null
    while (reader.readLine().also { line = it } != null) {
        writer.append(line).append('\n')
    }
    writer.flush() // i've used a buffered writer, so make sure to flush to the

    // underlying zip output stream
    zipStream.closeEntry()
    zipStream.finish()

    reader.close()
    writer.close()
}

private fun getBitmap(format: Int, buffer: ByteBuffer, w: Int, h: Int, rowStride: Int): Bitmap {
    val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
    if (format == PixelFormat.RGBA_8888) {
        buffer.rewind()
        bitmap.copyPixelsFromBuffer(buffer)
    } else if (format == ImageFormat.YUV_420_888) {
        val pixels =
            ByteBuffer.allocateDirect(w * h * 4).order(ByteOrder.nativeOrder()).asIntBuffer()
        for (y in 0 until h) {
            buffer.position(rowStride * y)
            for (x in 0 until w) {
                val value: Int = buffer.get().toInt() and 0xFF
                pixels.position(y * w + x)
                pixels.put(Color.argb(255, value, value, value))
            }
        }
        pixels.rewind()
        bitmap.copyPixelsFromBuffer(pixels)
    }
    return bitmap
}

// BANUBA_NOTE: saveImage is used just for dumping input ByteBuffer into the file.
private fun saveImage(context: Context, format: Int, buffer: ByteBuffer, w: Int, h: Int, rowStride: Int, filename: String) {
    val file = File(context.getExternalFilesDir(null), filename)

    val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
    if (format == PixelFormat.RGBA_8888) {
        buffer.rewind()
        bitmap.copyPixelsFromBuffer(buffer)
    } else if (format == ImageFormat.YUV_420_888) {
        val pixels =
            ByteBuffer.allocateDirect(w * h * 4).order(ByteOrder.nativeOrder()).asIntBuffer()
        for (y in 0 until h) {
            buffer.position(rowStride * y)
            for (x in 0 until w) {
                val value: Int = buffer.get().toInt() and 0xFF
                pixels.position(y * w + x)
                pixels.put(Color.argb(255, value, value, value))
            }
        }
        pixels.rewind()
        bitmap.copyPixelsFromBuffer(pixels)
    }

    try {
        FileOutputStream(file).use { out ->
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out) // bmp is your Bitmap instance
        }
    } catch (e: IOException) {
        e.printStackTrace()
        Log.e(BanubaImp.TAG, " Save E = " + e.message)
    }
    bitmap.recycle()
}