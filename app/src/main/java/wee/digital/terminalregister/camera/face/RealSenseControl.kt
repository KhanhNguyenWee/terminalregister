package wee.digital.terminalregister.camera.face

import android.graphics.Bitmap
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import com.intel.realsense.librealsense.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import org.opencv.core.Mat
import org.opencv.core.Rect
import wee.digital.library.util.Logger
import wee.digital.terminalregister.app.app
import wee.digital.terminalregister.camera.rgbToBitmapOpenCV
import wee.digital.terminalregister.camera.rgbToMatOpenCV
import wee.digital.terminalregister.camera.rgbToMatRectOpenCV
import wee.digital.terminalregister.camera.toBitmapNotRelease
import wee.digital.terminalregister.repository.DataCollect
import wee.digital.terminalregister.shared.Shared
import java.util.*

class RealSenseControl : DeviceListener {

    companion object {
        const val COLOR_WIDTH = 640
        const val COLOR_HEIGHT = 480
        const val DEPTH_WIDTH = 640
        const val DEPTH_HEIGHT = 480
        const val TIME_WAIT = 1000
        const val FRAME_RATE = 30
        const val FRAME_MAX_COUNT = 900000
        const val FRAME_MAX_SLEEP = -10
        const val SNAPSHOT_SKIP_FRAME = 5
        var DEPTH_UNIT = 0f
        var DEVICE_CONFIG = DeviceConfig()
    }

    private val log = Logger("RealSenseControl")
    var listener: Listener? = null
    var startListener: StartListener? = null
    private var mRSContext: RsContext? = null
    private var mPipeline: Pipeline? = null
    private var mPipelineProfile: PipelineProfile? = null
    private var isFrameOK = false
    private var mHandlerThread: HandlerThread? = null
    private var mHandler: Handler? = null
    private var mIsStreaming = false
    private var mDevice: Device? = null
    private var mFrameCount = FRAME_MAX_COUNT
    private var isPauseCamera = true
    private var isCheck = false
    private var isFrameProcessing = false
    var colorConfig = ColorConfig()
    var deviceConfig = DeviceConfig()
    private var mColorSensor: ColorSensor? = null
    private var mAlign: Align? = null
    private val mColorizerOrg = Colorizer().apply {
        /*0 - Jet
        1 - Classic
        2 - WhiteToBlack
        3 - BlackToWhite
        4 - Bio
        5 - Cold
        6 - Warm
        7 - Quantized
        8 - Pattern*/
        setValue(Option.COLOR_SCHEME, 0f)
    }
    val config = Config().apply {
        enableStream(
            StreamType.COLOR,
            0,
            COLOR_WIDTH,
            COLOR_HEIGHT,
            StreamFormat.RGB8,
            FRAME_RATE
        )
        enableStream(
            StreamType.DEPTH,
            0,
            DEPTH_WIDTH,
            DEPTH_HEIGHT,
            StreamFormat.Z16,
            FRAME_RATE
        )
    }
    private var isStopCamera = false
    var isGetDepthFrame = false

    private val mStreaming: Runnable by lazy {
        object : Runnable {
            @Throws
            @Synchronized
            override fun run() {
                var errorMessage = ""
                val isNext = try {
                    if (!isFrameProcessing) {
                        isFrameProcessing = true
                        FrameReleaser().use { fr ->
                            val frames: FrameSet =
                                mPipeline!!.waitForFrames(TIME_WAIT).releaseWith(fr)
                            val colorFrame = frames.first(StreamType.COLOR).releaseWith(fr)
                            val alignFrame = if (mAlign != null) mAlign!!.process(frames)
                                .releaseWith(fr) else frames
                            val colorizerFrame =
                                alignFrame.applyFilter(mColorizerOrg).releaseWith(fr)
                            val depthFrame =
                                colorizerFrame.first(StreamType.DEPTH).releaseWith(fr)
                            frameProcessingCoroutine(colorFrame, depthFrame)
                        }
                    }
                    true
                } catch (e: Exception) {
                    log.d("mStreaming : ${e.message}")
                    errorMessage += " ${e.message}"
                    false
                }

                if (isNext && !isStopCamera) {
                    mHandler?.post(this)
                } else {
//                    try {
//                        mPipeline?.stop()
//                        mPipeline?.close()
//                        mPipeline = null
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
                    mHandler?.removeCallbacks(this)
                    isFrameOK = false
                    log.d("mStreaming : $errorMessage")
                    if (errorMessage.isNotEmpty() && !isPauseCamera) {
                        hardwareReset()
                    } else {
                        mRSContext?.close()
                        mRSContext = null
                    }
                    listener?.onCameraError(errorMessage)
                    startListener?.onError(errorMessage)
                }
            }
        }
    }
    private var mProcessThread = HandlerThread("Processing....")
    private var mHandlerProcess = Handler()

    // -- SnapShot
    private var mSnapShotListener: SnapShotListener? = null
    private var isStopSnapShot = false
    private var mSnapShotSkipFrame = SNAPSHOT_SKIP_FRAME
    private var mSnapShotProcessing = false

    init {
        mHandlerThread = HandlerThread("Streaming...")
        mHandlerThread?.start()
        mHandler = Handler(mHandlerThread!!.looper)
        mHandlerThread?.uncaughtExceptionHandler =
            Thread.UncaughtExceptionHandler { t, e -> log.d("init : ${t.name} - ${e.message}") }

        mProcessThread.start()
        mHandlerProcess = Handler(mProcessThread.looper)
    }

    private val rectCrop: Rect = getRectCrop()

    private fun getRectCrop(): Rect {
        val w = COLOR_WIDTH
        val h = COLOR_HEIGHT
        val sh = 3
        val sw = 4
        val newH = h
        val newW = h * sw / sh
        val minus = (w - newW) / 2
        return Rect(minus, 0, newW, newH)
    }

    private suspend fun getFrameCoroutine(colorFrame: Frame, depthFrame: Frame?) {
        return coroutineScope {
            val color = async(Dispatchers.IO) {
                val c = colorFrame.rgbToMatRectOpenCV(rectCrop)
                c
            }
            val depth = async(Dispatchers.IO) {
                depthFrame?.rgbToMatRectOpenCV(rectCrop)
            }
            val matColor = color.await()
            val matDepth = depth.await()
            val bmColorJob = async(Dispatchers.IO) {
                matColor?.toBitmapNotRelease()
            }
            val bmDepthJob = async(Dispatchers.IO) {
                matDepth?.toBitmapNotRelease()
            }
            val bmColor = bmColorJob.await()
            if (mSnapShotListener != null) {
                val bmDepth = bmDepthJob.await()
                if (bmColor != null) {
                    mSnapShotListener?.onCapturedImage(bmColor.copy(bmColor.config, true), bmDepth)
                        ?.let {
                            mSnapShotProcessing = false
                            isStopSnapShot = false
                            mSnapShotListener = null
                        }
                }
            }
            Shared.imageLiveData.postValue(bmColor?.copy(bmColor.config, true))
            Log.e("Frame","Processing...")
            isFrameProcessing = false
            listener?.onCameraData(matColor?.clone(), matDepth?.clone(), null)
            matColor?.release()
            matDepth?.release()
            bmColor?.recycle()
        }
    }

    private fun frameProcessingCoroutine(
        colorFrame: Frame,
        depthColorFrame: Frame?,
        depthValueFrame: Frame? = null
    ) {
        try {
            if (!isStopSnapShot) {
                runBlocking {
                    getFrameCoroutine(colorFrame, depthColorFrame)
                }
            } else {
                mSnapShotSkipFrame--
                if (mSnapShotSkipFrame < 0) {
                    mSnapShotListener?.onCapturedImage(
                        colorFrame.rgbToBitmapOpenCV(),
                        depthColorFrame?.rgbToBitmapOpenCV()
                    )?.let {
                        stopStreamThread()
                        mSnapShotProcessing = false
                        isStopSnapShot = false
                        mSnapShotListener = null
                    }
                } else {
                    isFrameProcessing = false
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
            mSnapShotListener?.onError(e.message.toString())
        }
    }

    fun startStreamThread() {
        log.e("startStreamThread")
        if (mIsStreaming) return
        try {
            mIsStreaming = true
            isStopCamera = false
            isPauseCamera = true
            isCheck = true
            if (mRSContext != null) {
                mRSContext?.removeDevicesChangedCallback()
                mRSContext?.close()
                mRSContext = null
            }
            mRSContext = RsContext()
            mRSContext?.setDevicesChangedCallback(this)
            val isD400 = mRSContext?.queryDevices(ProductLine.D400)?.deviceCount ?: 0 > 0
            if (!isD400) {
                mAlign = Align(StreamType.COLOR)
            }
            mPipeline = Pipeline(mRSContext)

        } catch (e: Exception) {
            startListener?.onError("${e.message}")
            log.d("startStreamThread : ${e.message}")
        }
    }

    fun onResume() {
        log.e("OnResume")
        if (!isPauseCamera) return
        isPauseCamera = false
        isFrameProcessing = false
        configAndStart()
    }

    fun stopStreamThread() {
        log.e("stopStreamThread")
        if (!mIsStreaming) return
        log.d("stopStreamThread : try stop streaming")
        try {
            mIsStreaming = false
            isStopCamera = true
            isFrameProcessing = false
            onPause()
            mDevice?.close()
            mPipelineProfile?.close()
            mPipeline?.close()
            log.d("stopStreamThread : streaming stopped successfully")
        } catch (e: java.lang.Exception) {
            log.d("stopStreamThread : failed to stop streaming : ${e.message}")
            startListener?.onError("${e.message}")
        }
    }

    fun onPause() {
        log.e("onPause")
        if (isPauseCamera) return
        isPauseCamera = true
        val timeIn = System.currentTimeMillis()
        try {
            mPipeline?.stop()
            log.d("Pause Camera [${System.currentTimeMillis() - timeIn}]")
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun hasFace() {
        // Reset sleep time when has face
        mFrameCount = FRAME_MAX_COUNT
    }

    private fun configAndStart() {
        val now = System.currentTimeMillis()
        mPipelineProfile = try {
            mPipeline?.start(config)
        } catch (e: Throwable) {
            e.printStackTrace()
            null
        }
        mPipelineProfile ?: return
        val time = System.currentTimeMillis() - now
        log.d("configAndStart $time")
        mDevice = mPipelineProfile?.device
        mHandler?.post(mStreaming)
        val firmware = mDevice?.getInfo(CameraInfo.FIRMWARE_VERSION)
        val name = mDevice?.getInfo(CameraInfo.NAME)
        val productID = mDevice?.getInfo(CameraInfo.PRODUCT_ID)
        val serialNum = mDevice?.getInfo(CameraInfo.SERIAL_NUMBER)
        deviceConfig.apply {
            this.firmware = firmware ?: ""
            this.id = productID ?: ""
            this.serial = serialNum ?: ""
            this.name = name ?: ""
        }
        DEVICE_CONFIG = deviceConfig
        printInfo(mDevice!!)
        listener?.onCameraStarted()
        log.d("configAndStart : streaming started successfully")

    }

    private fun hardwareReset() {
        log.d("hardwareReset")
        mDevice?.hardwareReset()
    }

    fun applyColorConfig(option: Option, value: Float) {
        mColorSensor ?: return
        try {
            mColorSensor?.setValue(option, value)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getCurConfig(option: Option): Float {
        return mColorSensor!!.getValue(option)
    }

    private fun printInfo(device: Device) {
        device.querySensors()?.forEach { sensor ->
            log.d("printInfo : Sensor $sensor")
            if (sensor != null) {
                try {
                    if (sensor.`is`(Extension.COLOR_SENSOR)) {
                        mColorSensor = sensor.`as`(Extension.COLOR_SENSOR)
                        val brightness = mColorSensor!!.getValue(Option.BRIGHTNESS)
                        log.d("brightness $brightness - Setting.....")
                        val exposure = mColorSensor!!.getValue(Option.EXPOSURE)
                        log.d("exposure $exposure - Setting.....")
                        val contrast = mColorSensor!!.getValue(Option.CONTRAST)
                        log.d("contrast $contrast - Setting.....")
                        val saturation = mColorSensor!!.getValue(Option.SATURATION)
                        log.d("saturation $saturation - Setting.....")
                        val backLightCompensation =
                            mColorSensor!!.getValue(Option.BACKLIGHT_COMPENSATION)
                        log.d("backLight_compensation $backLightCompensation - Setting.....")
                        val gain = mColorSensor!!.getValue(Option.GAIN)
                        log.d("gain $gain - Setting.....")
                        val gamma = mColorSensor!!.getValue(Option.GAMMA)
                        log.d("gama $gamma - Setting.....")
                        val whiteBalance = mColorSensor!!.getValue(Option.WHITE_BALANCE)
                        log.d("white_balance $whiteBalance - Setting.....")
                        val sharpness = mColorSensor!!.getValue(Option.SHARPNESS)
                        log.d("sharpness $sharpness - Setting.....")
                        val hue = mColorSensor!!.getValue(Option.HUE)
                        log.d("hue $hue - Setting.....")
                        val enableAutoExposure =
                            mColorSensor!!.getValue(Option.ENABLE_AUTO_EXPOSURE)
                        log.d("enable_auto_exposure $enableAutoExposure - Setting.....")
                        val enableAutoWhiteBalance =
                            mColorSensor!!.getValue(Option.ENABLE_AUTO_WHITE_BALANCE)
                        log.d("auto_white_balance $enableAutoWhiteBalance - Setting.....")
                        colorConfig.apply {
                            this.brightness = brightness
                            this.exposure = exposure
                            this.contrast = contrast
                            this.saturation = saturation
                            this.backlight = backLightCompensation
                            this.gain = gain
                            this.gamma = gamma
                            this.sharpness = sharpness
                            this.hue = hue
                            this.autoExposure = enableAutoExposure
                            this.autoWhiteBalance = enableAutoWhiteBalance
                        }
                    }
                } catch (e: Exception) {
                    log.d("Error: ${e.message}")
                }
            }
        }
    }

    fun getSnapShot(listener: SnapShotListener) {
        if (mSnapShotProcessing) return
        mSnapShotProcessing = true
        mSnapShotListener = listener
        mSnapShotSkipFrame = SNAPSHOT_SKIP_FRAME
        if (!mIsStreaming) {
            isStopSnapShot = true
            startStreamThread()
        } else {
            isStopSnapShot = false
        }
    }

    interface Listener {
        fun onCameraStarted() {}
        fun onCameraError(mess: String) {}
        fun onCameraData(colorBitmap: Mat?, depthBitmap: Mat?, dataCollect: DataCollect?)
    }

    interface SnapShotListener {
        fun onCapturedImage(colorBM: Bitmap?, depthBM: Bitmap?)
        fun onError(message: String)
    }

    interface StartListener {
        fun onStart()
        fun onError(mess: String)
    }

    data class ColorConfig(
        var backlight: Float = 0f,
        var brightness: Float = 0f,
        var contrast: Float = 0f,
        var exposure: Float = 0f,
        var gain: Float = 0f,
        var gamma: Float = 0f,
        var hue: Float = 0f,
        var saturation: Float = 0f,
        var sharpness: Float = 0f,
        var autoExposure: Float = 0f,
        var autoWhiteBalance: Float = 0f
    )

    data class DeviceConfig(
        var name: String = "",
        var serial: String = "",
        var id: String = "",
        var firmware: String = ""
    )

    override fun onDeviceAttach() {
        log.d("onDeviceAttach")
        startStreamThread()
    }

    override fun onDeviceDetach() {
        log.d("onDeviceDetach")
        stopStreamThread()
    }

}