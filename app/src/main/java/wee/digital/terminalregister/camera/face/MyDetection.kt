package wee.digital.terminalregister.camera.face

import android.graphics.Bitmap
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.Face
import org.opencv.core.Mat
import wee.digital.library.util.Logger
import wee.digital.terminalregister.camera.*
import wee.digital.terminalregister.repository.DataCollect
import wee.digital.terminalregister.repository.DataGetFacePoint
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MyDetection {

    private val log = Logger("MyDetection")

    private var moduleFake: ModelFilter? = null

    private val executorFaceDetect: ExecutorService = Executors.newSingleThreadExecutor()

    private var isDetectFrame = false

    private var isFaceEligible = false

    private var countFaceNotFrame = 0

    private var countFaceEligible = 0

    var listener: DetectionCallBack? = null

    init {
        try {
            moduleFake = ModelFilter("depth/manifest.json")
        } catch (e: Exception) {
            log.d("initModule Error: ${e.message}")
        }
    }

    fun mlKitDetectFrame(fullFrameColor: Mat, frameDepth: Mat, dataCollect: DataCollect?) {
        if (isDetectFrame) return
        isDetectFrame = true
        executorFaceDetect.submit {
            val bitmapColor = fullFrameColor.clone().matResize()?.toBitmap()
            if (bitmapColor == null) {
                resetDetect()
            } else {
                val img = InputImage.fromBitmap(bitmapColor, 0)
                MLKitDetector.detector.process(img)
                    .addOnSuccessListener {
                        try {
                            executorFaceDetect.submit {
                                val face = it.getLargestMlKitFace()
                                if (face == null) {
                                    resetDetect()
                                } else {
                                    optionFace(face, fullFrameColor, frameDepth, dataCollect)
                                }
                            }
                        } catch (e: Exception) {
                            resetDetect()
                        }
                    }
                    .addOnFailureListener {
                        resetDetect()
                    }

            }

        }
    }

    private fun optionFace(
        face: Face,
        fullFrameColor: Mat,
        frameDepth: Mat?,
        dataCollect: DataCollect?
    ) {
        if (face.checkFaceOke() && face.boundingBox.checkZoneHDFaceMlKit()) {
            val depthDataResize = frameDepth?.matResize()
            val colorRaw = fullFrameColor.toBitmap()
            if (depthDataResize != null && colorRaw != null) {
                log.d("option face ok")
                val byteFullFrame = BitmapUtils.bitmapToByteArray(colorRaw)
                val frameCheckFake =
                    depthDataResize.toBitmap()?.cropRect(face.boundingBox.paddingMTCNN())
                val data = colorRaw.getDataFaceAndFace(face)
                    .apply { this.fullFrame = byteFullFrame }
                if (frameCheckFake != null && data.face != null && data.dataFace != null && data.fullFrame != null) {
                    listener?.faceOnFrame()
                    dataCollect?.dataFacePoint = data
                    fakeFace(face, data, dataCollect, frameCheckFake)
                } else {
                    log.d("get data face null")
                    resetDetect()
                }
                isDetectFrame = false
            } else {
                log.d("check bitmap color, 3d null")
                resetDetect()
            }
        } else {
            log.d("option face fail")
            resetDetect()
        }

    }

    private fun resetDetect() {
        isDetectFrame = false
        countFaceEligible = 0
        countFaceNotFrame++.also {
            if (it <= 3) return@also
            countFaceNotFrame = 0
            listener?.faceNotFrame()
        }

    }

    private fun fakeFace(
        face: Face,
        dataFace: DataGetFacePoint,
        dataCollect: DataCollect?,
        frameCheckFake: Bitmap
    ) {
        if (isFaceEligible) return
        isFaceEligible = true
        moduleFake?.processImage(frameCheckFake) { text, _ ->
            executorFaceDetect.submit {
                frameCheckFake.recycle()
                log.d("fake face : $text")
                if (text != "real") {
                    resetFaceEligible()
                } else {
                    countFaceEligible++
                    if (countFaceEligible > 3) {
                        countFaceEligible = 0
                        listener?.faceEligible(face, dataFace, dataCollect)
                    }
                    isFaceEligible = false
                }
            }
        }
    }


    private fun resetFaceEligible() {
        isFaceEligible = false
        countFaceEligible = 0
        listener?.faceNotEligible()
    }

    fun destroyDetection() {
        try {
            executorFaceDetect.shutdown()
            moduleFake?.destroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface DetectionCallBack {
        fun faceNotFrame() {}
        fun faceOnFrame() {}
        fun multiFaceOnFrame() {}
        fun faceNotEligible() {}
        fun faceEligible(face: Face, dataFace: DataGetFacePoint, dataCollect: DataCollect?) {}
    }

}
