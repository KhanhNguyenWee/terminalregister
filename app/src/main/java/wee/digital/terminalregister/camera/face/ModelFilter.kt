package wee.digital.terminalregister.camera.face

import android.graphics.Bitmap
import com.google.mlkit.common.model.LocalModel
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.label.ImageLabeler
import com.google.mlkit.vision.label.ImageLabeling
import com.google.mlkit.vision.label.custom.CustomImageLabelerOptions

class ModelFilter(fileName: String) {

    private var imageLabeler: ImageLabeler? = null

    private var isChecking: Boolean = false

    init {
        try {
            val localModel = LocalModel.Builder()
                .setAssetManifestFilePath(fileName)
                .build()
            val option = CustomImageLabelerOptions.Builder(localModel)
                .setConfidenceThreshold(0.5f).build()
            imageLabeler = ImageLabeling.getClient(option)
        } catch (e: Exception) {
            print("")
        }
    }

    @Synchronized
    fun processImage(bitmap: Bitmap, onResult: (String?, Float) -> Unit) {
        if (isChecking)return
        isChecking = true
        try {
            val image = InputImage.fromBitmap(bitmap, 0)
            imageLabeler?.process(image)
                ?.addOnSuccessListener {
                    val label = it.firstOrNull()
                    onResult(label?.text, label?.confidence ?: 100f)
                    isChecking = false
                }
                ?.addOnFailureListener {
                    isChecking = false
                    onResult("", 100f)
                }

        } catch (e: Exception) {
            isChecking = false
            onResult("", 100f)
        }
    }

    fun destroy() {
        imageLabeler?.close()
    }

}
