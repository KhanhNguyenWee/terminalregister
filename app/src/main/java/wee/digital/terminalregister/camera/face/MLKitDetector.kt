package wee.digital.terminalregister.camera.face

import android.util.Size
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions

object MLKitDetector {
    const val MIN_BLUR = 10000000.0

    const val RESIZE_BLUR = 0.25

    const val RESIZE = 480.0

    const val MIN_SIZE = 22

    const val MIN = -30f

    const val MAX = 30f

    const val maxZ = 10f

    private const val minY = -30f

    private const val maxY = 30f

    private const val minZ = -10f

    val rangeX = MIN..MAX

    val rangeY = minY..maxY

    val rangeZ = minZ..maxZ

    var size = Size(1280, 720)

    val width get() = size.width

    val height get() = size.height

    private val faceDetectionOption: FaceDetectorOptions
        get() = FaceDetectorOptions.Builder()
            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
//            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
            .setMinFaceSize(0.4f)
            .enableTracking()
            .build()

    val detector by lazy {
        FaceDetection.getClient(faceDetectionOption)
    }

}