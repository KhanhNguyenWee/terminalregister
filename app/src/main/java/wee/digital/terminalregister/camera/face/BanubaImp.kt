package wee.digital.terminalregister.camera.face

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.util.Range
import android.util.Size
import android.view.SurfaceView
import com.banuba.sdk.camera.Facing
import com.banuba.sdk.effect_player.*
import com.banuba.sdk.entity.ContentRatioParams
import com.banuba.sdk.entity.RecordedVideoInfo
import com.banuba.sdk.manager.*
import com.banuba.sdk.types.Data
import com.banuba.sdk.types.FullImageData
import org.opencv.core.Mat
import wee.digital.terminalregister.camera.checkEffectAsset
import wee.digital.terminalregister.camera.checkEffectFromFirebase
import wee.digital.terminalregister.camera.toBitmap
import wee.digital.library.extension.readAsset
import wee.digital.library.util.Logger
import wee.digital.terminalregister.app.app
import wee.digital.terminalregister.repository.model.EffectFirebaseData
import java.io.*

class BanubaImp(val context: Context) {

    companion object {
        val TAG = "BanubaImpl"
        val tokenBanuba: String = readAsset("WeeDigital_token.txt")
        val SIZE_FRAME = Size(640,480)
        val FRAME_RANGE = Range(15, 30)
    }

    private var number = -1L

    private var effect: Effect? = null

    private var isRecording = false

    var listActiveEffect = arrayListOf<EffectFirebaseData>()

    var hasEffect: Boolean = false

    var effectActivatedListener: EffectActivatedListener? = null

    private var recordVideoListener: RecordVideoListener? = null

    private val log = Logger("BanubaImp")

    private val cameraOrientation: CameraOrientation = CameraOrientation.DEG_0

    private val mOrientation = FullImageData.Orientation(cameraOrientation, false, 0)

    private var sdkManager: BanubaSdkManager

    private val maskUri by lazy(LazyThreadSafetyMode.NONE) {
        Uri.parse(BanubaSdkManager.getResourcesBase())
            .buildUpon()
            .appendPath("effects")
            .appendPath("WeeDigitalScan3")
            .build()
    }

    private val none by lazy(LazyThreadSafetyMode.NONE) {
        Uri.parse(BanubaSdkManager.getResourcesBase())
            .buildUpon()
            .appendPath("effects")
            .appendPath("none")
            .build()
    }

    init {
        // BANUBA_NOTE: using setResolutionController you can change the Banuba SDK preferred render size.
        // IMPORTANT: The order of width and height in the returned resolution callback result is important.
        // The incorrect order may be the reason of any side effects (blurring or zooming for example).
        // Try to change the order and or resolution controller returned Size parameters - maybe it helps to solve your blurring issue!
        val banubaSDKManagerConfiguration = BanubaSdkManagerConfiguration.newInstance()
            .setFpsController { _, _ -> FRAME_RANGE }
            .setAutoRotationHandler { false }
            .setResolutionController { SIZE_FRAME }
            .build()
        sdkManager = BanubaSdkManager(context, banubaSDKManagerConfiguration)

        // BANUBA_NOTE: implementation of IEventCallback interface is needed just for onFrameRendered overriding
        sdkManager.setCallback(object : IEventCallback {
            override fun onCameraOpenError(p0: Throwable) {
            }

            override fun onCameraStatus(p0: Boolean) {
            }

            override fun onScreenshotReady(p0: Bitmap) {
            }

            override fun onHQPhotoReady(p0: Bitmap) {
            }

            override fun onVideoRecordingFinished(videoInfo: RecordedVideoInfo) {
                Log.d(
                    TAG, "Video recording finished. Recorded file = ${videoInfo.filePath}," +
                        "duration = ${videoInfo.recordedLength}")
                recordVideoListener?.onDone(File(videoInfo.filePath))
                isRecording = false
            }

            override fun onVideoRecordingStatusChange(isStarted: Boolean) {
                Log.d(TAG, "Video recording status changed. isRecording = $isStarted")
                isRecording = isStarted
            }

            override fun onImageProcessed(p0: Bitmap) {
            }

            // BANUBA_NOTE: the main idea of the onFrameRendered overriding here
            // is to compare the input frame size from Camera with returned processed frame size.
            // If the input and returned sizes are not equal - it seems that this is the reason of blurring size effect.
            // In your cases of external Camera using the input frame size can be logged in pushFrameRealSenseBitmap method.
            override fun onFrameRendered(data: Data, width: Int, heigth: Int) {
//                Log.i(TAG, "[===sdk] onFrameRendered w: $width h: $heigth")

                // BANUBA_NOTE: the returned data is dumped into camera.jpg file in the External app directory.
                // For this application the path is /sdcard/Android/data/com.example.realsense_banuba/files/.
                // Pay attention that each returned data is dumped - it can be the reason of corrupted
                // file when camera.jpg is opened by tap on it via Android Studio Device File Explorer.
                // To same several files any counter with interval number can be used ("camera_$i.jpg")
//                saveImage(
//                    PixelFormat.RGBA_8888,
//                    data.data,
//                    width,
//                    heigth,
//                    width,
//                    "camera_${System.currentTimeMillis()}.jpg"
//                )

//                val bitmapShow = getBitmap(PixelFormat.RGBA_8888, data.data, width, heigth, width)
//                mSurfaceView?.draw(Canvas(bitmapShow))
                // BANUBA_NOTE: data.close() is needed to prevent memory leak!
                data.close()
            }
        })
    }

    private fun runRenderThread(block: () -> Unit = {}) {
        sdkManager.runOnRenderThread { block() }
    }

    fun checkEffect(block: () -> Unit) {
        checkEffectAsset(context, block)
    }

    fun checkEffectFirebase(listEffect: ArrayList<EffectFirebaseData>, block: () -> Unit){
        listActiveEffect.clear()
        listEffect.forEach {
            if(it.isUse) listActiveEffect.add(it)
        }
        checkEffectFromFirebase(app.applicationContext, listEffect,block)
    }

    fun start(surfaceView: SurfaceView) {
        number = -1L
        sdkManager.attachSurface(surfaceView)
//        mSurfaceView = surfaceView
//        mSurfaceView?.doOnLayout {
//            log.d("LayoutSurface: ${it.width}x${it.height}")
//        }
        // BANUBA_NOTE: sdkManager.openCamera() here is used only to show example work with Banuba SDK Camera2 implementation
//        sdkManager.openCamera()
        // BANUBA_NOTE: startForwardingFrames() is used just for capturing processed camera frames by Banuba SDK.
        // See the IEventCallback.onFrameRendered method from com.banuba.sdk.manager that is overridden above in init section
        sdkManager.startForwardingFrames()
    }

    fun loadAllEffect() {
        sdkManager.loadEffects()
    }

    fun pauseEffect() {
        sdkManager.effectPlayerPause()
    }

    fun playEffect() {
        sdkManager.effectPlayerPlay()
    }

    fun destroy() {
        sdkManager.closeCamera()
        sdkManager.stopForwardingFrames()
        sdkManager.onSurfaceDestroyed()
        sdkManager.releaseSurface()
    }

    fun recycle() {
        sdkManager.recycle()
    }

    fun applyEffect(block: () -> Unit = {}) {
        try {
            if (hasEffect) {
                block()
                return
            }
            sdkManager.effectManager.removeEffectActivatedListener(effectActivatedListener)
            effectActivatedListener = EffectActivatedListener {
                log.d("applyEffect : Activated $it")
                hasEffect = true
                block()
            }
            sdkManager.effectManager.addEffectActivatedListener(effectActivatedListener)
            sdkManager.loadEffect(maskUri.toString(), true)
        } catch (e: Throwable) {
            log.e("applyEffect : ${e.message}")
            println(e.message)
            effect = null
            hasEffect = false
        }
    }

    fun unApplyEffect(block: () -> Unit = {}) {
        try {
            if (!hasEffect) {
                block()
                return
            }
            sdkManager.effectManager.removeEffectActivatedListener(effectActivatedListener)
            sdkManager.loadEffect("", true)?.let {
                log.d("unApplyEffect : Activated $it")
                hasEffect = false
                block()
            }
        } catch (e: Throwable) {
            log.e("unApplyEffect : ${e.message}")
            hasEffect = false
            println(e.message)
        }
    }

    fun switchCamera() {
        var isMirror = true
        val facing = when (sdkManager.cameraFacing) {
            Facing.FRONT -> {
                isMirror = false
                Facing.BACK
            }
            else -> {
                Facing.FRONT
            }
        }
        sdkManager.setCameraFacing(facing, isMirror)
    }

    fun startRecordVideo(listener: RecordVideoListener?){
        if(isRecording) return
        isRecording = true
        try{
            recordVideoListener = null
            val vPath = generateVideoFilePath()
            sdkManager.startVideoRecording(
                vPath,
                false, // Record with audio
                ContentRatioParams(SIZE_FRAME.width, SIZE_FRAME.height, false),
                1f // speed
            )
            recordVideoListener = listener
        }catch (e: Exception){
            e.printStackTrace()
            isRecording = false
        }finally {
            recordVideoListener?.onStart()
        }

    }

    private fun generateVideoFilePath(): String = File(context.filesDir,
        "effect_video_${System.currentTimeMillis()}.mp4").absolutePath

    fun stopRecordVideo(){
        sdkManager.stopVideoRecording()
    }

    fun takePic(block: (bitmap: Bitmap) -> Unit) {
        sdkManager.takeEditedImage()
    }

    fun pushFrameRealSense(fullImageData: FullImageData, number: Long) {
        sdkManager.effectPlayer.pushFrameWithNumber(fullImageData, number)
    }

    fun pushFrameRealSenseBitmap(mat: Mat) {
        runRenderThread {
            number++
            val bitmap = mat.toBitmap()

            // BANUBA_NOTE: logging of input frame size from Camera.
            Log.i(TAG, "[===sdk] pushFrameRealSenseBitmap w: ${bitmap?.width} h: ${bitmap?.height}")

            val fullImageData = FullImageData(bitmap, mOrientation)
            synchronized(number) {
                pushFrameRealSense(fullImageData, number)
            }
        }

    }

    fun pushBitmap(bitmap: Bitmap){
        runRenderThread {
            try{
                number++
                // BANUBA_NOTE: logging of input frame size from Camera.
                Log.i(TAG, "[===sdk] pushFrameRealSenseBitmap w: ${bitmap.width} h: ${bitmap.height}")
                val fullImageData = FullImageData(bitmap.copy(bitmap.config,true), mOrientation)
                synchronized(number) {
                    pushFrameRealSense(fullImageData, number)
                }
            }catch (e: Exception){
                e.printStackTrace()
            }finally {
                bitmap.recycle()
            }

        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun applyTouchEffect(surfaceView: SurfaceView) {
        val touchListener = BanubaSdkTouchListener(surfaceView.context, sdkManager.effectPlayer)
        surfaceView.setOnTouchListener(touchListener)
    }

    fun readAsset(filename: String): String {
        val sb = StringBuilder()
        BufferedReader(InputStreamReader(app.assets.open(filename))).useLines { lines ->
            lines.forEach {
                sb.append(it)
            }
        }
        return sb.toString()
    }

    private fun getActiveEffect(): String{
        val listReadyEffect = sdkManager.loadEffects()
        return if(listActiveEffect.isNotEmpty()){
            val effect = listActiveEffect.random()
            var active = ""
            listReadyEffect.forEach {
                if(it.path == effect.name){
                    active = Uri.parse(BanubaSdkManager.getResourcesBase())
                        .buildUpon()
                        .appendPath("effects")
                        .appendPath(effect.name)
                        .build().toString()
                    return@forEach
                }
            }
            active
        }else{
            ""
        }
    }

    interface RecordVideoListener{
        fun onStart()
        fun onDone(videoFile: File)
    }

}