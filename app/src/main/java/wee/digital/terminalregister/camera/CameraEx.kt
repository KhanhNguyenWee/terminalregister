package wee.digital.terminalregister.camera

import android.graphics.*
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import androidx.core.graphics.toPoint
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceLandmark
import com.intel.realsense.librealsense.Extension
import com.intel.realsense.librealsense.Frame
import com.intel.realsense.librealsense.VideoFrame
import org.opencv.android.Utils
import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import wee.digital.terminalregister.camera.face.MLKitDetector
import wee.digital.terminalregister.camera.face.RealSenseControl
import wee.digital.library.util.Logger
import wee.digital.terminalregister.camera.face.Box
import wee.digital.terminalregister.repository.DataCollect
import wee.digital.terminalregister.repository.DataGetFacePoint
import wee.digital.terminalregister.repository.FacePointData
import java.util.*
import java.util.regex.Pattern
import kotlin.math.*

data class ListLargestFaceData(
    val faceLarge : Box?,
    val faceSmall : Box?
)

private val log = Logger("CameraEx")

var ratio = 1.0

/**
 * rotation bitmap [rotateBitmap]
 */
fun Bitmap.rotateBitmap(degree: Float): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(degree)
    return Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true)
}

/**
 * get rectDepthFace [getRectDepthFace], [getRectDepthFaceFullHD]
 */
fun Rect.getRectDepthFace(): Rect {
    val leftCorner = 77
    val topCorner = 60
    val scale = 0.76
    val x = this.exactCenterX() * scale + leftCorner / scale
    val y = this.exactCenterY() * scale + topCorner
    val width = this.width() * scale
    val height = this.height() * scale
    val left = x - width / 2
    val top = y - height / 2
    val right = x + width / 2
    val bottom = y + height / 2
    return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
}

fun Rect.getRectDepthFaceFullHD(): Rect {
    val leftCorner = 1
    val topCorner = 35
    val scale = 0.36
    val x = this.exactCenterX() * scale + leftCorner / scale
    val y = this.exactCenterY() * scale + topCorner
    val width = this.width() * scale
    val height = this.height() * scale
    val left = x - width / 2
    val top = y - height / 2
    val right = x + width / 2
    val bottom = y + height / 2
    return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
}

/**
 * crop bitmap width rect [cropBitmapWithFace], [cropBitmapWithRect]
 */
fun Bitmap?.cropBitmapWithFace(box: Box): Bitmap? {
    this ?: return null
    val rect = box.transform2Rect()
    var top = rect.top
    if (top < 0) {
        top = 0
    }
    var left = rect.left
    if (left < 0) {
        left = 0
    }

    val height = rect.height()
    val width = rect.width() + (height / 10)
    var x = left
    var y = top
    if (x < 0) x = 0
    if (y < 0) y = 0
    return try {
        if (y + height > this.height || x + width > this.width) {
            val cropBitmap = this.copy(Bitmap.Config.ARGB_8888, true)
            this.recycle()
            cropBitmap
        } else {
            val cropBitmap = Bitmap.createBitmap(this, x, y, width, height)
            this.recycle()
            cropBitmap
        }
    } catch (ex: Exception) {
        log.d("cropBitmapWithFace : ${ex.message}")
        this.recycle()
        null
    }
}

fun Bitmap?.cropFace(box: Box): Bitmap? {
    this ?: return null
    val rect = box.transform2Rect()
    val height = rect.height()
    val width = rect.width()
    val top = if (rect.top < 0) 0 else rect.top
    val left = if (rect.left < 0) 0 else rect.left
    val rectCrop = this.getRectCrop(Rect(left, top, left + width, top + height))
    val copiedBitmap = this.copy(Bitmap.Config.RGB_565, true)
    return try {
        Bitmap.createBitmap(
            copiedBitmap,
            rectCrop.left,
            rectCrop.top,
            rectCrop.width(),
            rectCrop.height()
        )
    } catch (ex: Exception) {
        copiedBitmap
    }
}

private fun Bitmap.getRectCrop(rect: Rect): Rect {
    val top = if (rect.top < 0) 0 else rect.top
    val left = if (rect.left < 0) 0 else rect.left
    val right = if (rect.right > this.width) this.width else rect.right
    val bottom = if (rect.bottom > this.height) this.height else rect.bottom
    return Rect(left, top, right, bottom)
}

fun Bitmap?.cropBitmapWithRect(rect: Rect): Bitmap? {
    this ?: return null
    var top = rect.top
    if (top < 0) {
        top = 0
    }
    var left = rect.left
    if (left < 0) {
        left = 0
    }
    val height = rect.height()
    val width = rect.width()
    var x = left
    var y = top
    if (x < 0) x = 0
    if (y < 0) y = 0
    val rectCrop = Rect(left, top, x + width, y + height)
    return try {
        val cropBitmap = Bitmap.createBitmap(
            this,
            rectCrop.left, rectCrop.top, rectCrop.width(), rectCrop.height()
        )
        cropBitmap
    } catch (ex: Exception) {
        log.d("cropBitmapWithRect : ${ex.message}")
        null
    }
}

fun Bitmap?.cropFaceWithPadding(rect: Rect): Bitmap? {
    this ?: return null
    val extraH = 0.5f
    val extraW = 0.5f
    val plusH = rect.height() * extraH
    val plusW = rect.width() * extraW
    val height = rect.height() + plusH.roundToInt()
    val width = rect.width() + plusW.roundToInt()
    var top = rect.top - (plusH / 2).roundToInt()
    var left = rect.left - (plusW / 2).roundToInt()
    val copiedBitmap = this.copy(Bitmap.Config.RGB_565, true)
    return try {
        Bitmap.createBitmap(
            copiedBitmap,
            left,
            top,
            width,
            height
        )
    } catch (ex: Exception) {
        null
    }
}

/**
 * get dataPoint in face
 */
fun Bitmap?.getDataFaceAndFace(face: Face): DataGetFacePoint {
    this ?: return DataGetFacePoint(null, null)
    val rect = face.getRectRatio()

    val extraH = 0.2f
    val extraW = 0.2f

    val plusH = rect.height() * extraH
    val plusW = rect.width() * extraW

    val height = rect.height() + plusH.roundToInt()
    val width = rect.width() + plusW.roundToInt()
    val top = rect.top - (plusH / 2).roundToInt()
    val left = rect.left - (plusW / 2).roundToInt()

    val copiedBitmap = this.copy(Bitmap.Config.ARGB_8888, true)
    try {
        val bmFace = Bitmap.createBitmap(
            copiedBitmap,
            left,
            top,
            width,
            height
        )
        val facePointData =
            face.getDataFace(
                (rect.width() * 0.2f).toInt(),
                (rect.height() * 0.2f).toInt()
            )
        val byteFace = BitmapUtils.bitmapToByteArray(bmFace)
        this.recycle()
        bmFace?.recycle()
        return DataGetFacePoint(facePointData, byteFace)
    } catch (ex: Exception) {
        val facePointData = face.getDataPoint()
        val byteFace = BitmapUtils.bitmapToByteArray(this)
        this.recycle()
        return DataGetFacePoint(facePointData, byteFace)
    }
}

fun Face.getDataPoint(): FacePointData {
    val eyeLeft = this.getLandmark(FaceLandmark.LEFT_EYE)?.position?.getRatio()
    val eyeRight = this.getLandmark(FaceLandmark.RIGHT_EYE)?.position?.getRatio()
    val mouthLeft = this.getLandmark(FaceLandmark.MOUTH_LEFT)?.position?.getRatio()
    val mouthRight = this.getLandmark(FaceLandmark.MOUTH_RIGHT)?.position?.getRatio()
    val nose = this.getLandmark(FaceLandmark.NOSE_BASE)?.position?.getRatio()
    return FacePointData(
        this.getRectRatio(),
        eyeRight!!.toPoint(),
        eyeLeft!!.toPoint(),
        nose!!.toPoint(),
        mouthRight!!.toPoint(),
        mouthLeft!!.toPoint()
    )
}

fun Face.getDataFace(plusW: Int, plusH: Int): FacePointData {
    val faceRect = this.getRectRatio()
    val newRect =
        Rect(
            0 + plusW / 2,
            0 + plusH / 2,
            faceRect.width() + plusW / 2,
            faceRect.height() + plusH / 2
        )

    val eyeLeft = this.getLandmark(FaceLandmark.LEFT_EYE)?.position?.getRatio()
    val eyeRight = this.getLandmark(FaceLandmark.RIGHT_EYE)?.position?.getRatio()
    val mouthLeft = this.getLandmark(FaceLandmark.MOUTH_LEFT)?.position?.getRatio()
    val mouthRight = this.getLandmark(FaceLandmark.MOUTH_RIGHT)?.position?.getRatio()
    val nose = this.getLandmark(FaceLandmark.NOSE_BASE)?.position?.getRatio()

    return FacePointData(
        newRect,
        eyeRight?.toPoint().convertPoint(this.boundingBox, plusW, plusH),
        eyeLeft?.toPoint().convertPoint(this.boundingBox, plusW, plusH),
        nose?.toPoint().convertPoint(this.boundingBox, plusW, plusH),
        mouthRight?.toPoint().convertPoint(this.boundingBox, plusW, plusH),
        mouthLeft?.toPoint().convertPoint(this.boundingBox, plusW, plusH)
    )
}

private fun Point?.convertPoint(): Point {
    this ?: return Point(0, 0)
    val old_X = this.x
    val old_Y = this.y
    return Point(old_X, old_Y)
}

private fun Point?.convertPoint(faceRect: Rect, plusW: Int, plusH: Int): Point {
    this ?: return Point(0, 0)
    val old_X = this.x
    val old_Y = this.y
    val new_X = old_X - faceRect.left + plusW / 2
    val new_Y = old_Y - faceRect.top + plusH / 2
    return Point(new_X, new_Y)
}

/**
 * flipBitmap
 */
private fun Bitmap.flipBitmap(): Bitmap {
    val m = Matrix()
    m.preScale((-1).toFloat(), 1f)
    val dst: Bitmap = Bitmap.createBitmap(this, 0, 0, this.width, this.height, m, false)
    dst.density = DisplayMetrics.DENSITY_DEFAULT
    this.recycle()
    return dst
}

/**
 * convert frame camera realsense to bitmap [getBitmapFromFrame]
 */
private fun ByteArray.rgb8ToArgb(width: Int, height: Int): IntArray? {
    try {
        val frameSize = width * height
        val rgb = IntArray(frameSize)
        var index = 0
        for (j in 0 until height) {
            for (i in 0 until width) {
                val B = this[3 * index].toInt()
                val G = this[3 * index + 1].toInt()
                val R = this[3 * index + 2].toInt()
                rgb[index] = (R and 0xff) or (G and 0xff shl 8) or (B and 0xff shl 16)
                index++
            }
        }
        return rgb
    } catch (e: Exception) {
        log.d("rgb8ToArgb : ${e.message}")
        return null
    }
}

fun Frame?.getBitmapFromFrame(width: Int, height: Int): Bitmap? {
    return try {
        this ?: return null
        val data = ByteArray(this.dataSize)
        this.getData(data)
        val argb = data.rgb8ToArgb(width, height)
            ?: return null
        val bmp = Bitmap.createBitmap(argb, width, height, Bitmap.Config.RGB_565)
        return bmp.flipBitmap()
    } catch (e: OutOfMemoryError) {
        null
    } catch (e: Exception) {
        null
    }
}

fun ByteArray?.getBitmapFromFrame(width: Int, height: Int): Bitmap? {
    this ?: return null
    return try {
        val argb = this.rgb8ToArgb(width, height)
            ?: return null
        val bmp = Bitmap.createBitmap(argb, width, height, Bitmap.Config.RGB_565)
        return bmp.flipBitmap()
    } catch (e: OutOfMemoryError) {
        null
    } catch (e: Exception) {
        null
    }
}

fun Frame?.rgbToBitmapOpenCV(): Bitmap? {
    this ?: return null
    val mat = this.rgbToMatOpenCV()
    return mat?.toBitmap()
}

fun Frame.getData(): ByteArray {
    val videoFrame: VideoFrame = this.`as`(Extension.VIDEO_FRAME)
    val colourBuff = ByteArray(videoFrame.height * videoFrame.stride)
    videoFrame.getData(colourBuff)
    return colourBuff
}

fun ByteArray?.rgbToMatOpenCV(width: Int, height: Int): Mat? {
    this ?: return null
    return try {
        val mColour = Mat(height, width, CvType.CV_8UC3)
        mColour.put(0, 0, this)
        Core.flip(mColour, mColour, 1) // Mirror
        mColour
    } catch (e: Throwable) {
        null
    }
}

fun Frame?.rgbToMatRectOpenCV(rect: org.opencv.core.Rect): Mat? {
    this ?: return null
    return try {
        val videoFrame: VideoFrame = this.`as`(Extension.VIDEO_FRAME)
        val mColour = Mat(videoFrame.height, videoFrame.width, CvType.CV_8UC3)
        val colourBuff = ByteArray(videoFrame.height * videoFrame.stride)
        videoFrame.getData(colourBuff)
        mColour.put(0, 0, colourBuff)
        val cropMat = mColour.submat(org.opencv.core.Rect(rect.x, rect.y, rect.width, rect.height))
//            Core.transpose(mColour, mColour) // Rotate 90
        Core.flip(cropMat, cropMat, 1) // Mirror
        mColour.release()
        cropMat
    } catch (e: Throwable) {
        null
    }
}

fun Frame?.rgbToMatOpenCV(): Mat? {
    this ?: return null
    return try {
        val videoFrame: VideoFrame = this.`as`(Extension.VIDEO_FRAME)
        val mColour = Mat(videoFrame.height, videoFrame.width, CvType.CV_8UC3)
        val colourBuff = ByteArray(videoFrame.height * videoFrame.stride)
        videoFrame.getData(colourBuff)
        mColour.put(0, 0, colourBuff)
        //Core.transpose(mColour, mColour) // Rotate 90
        Core.flip(mColour, mColour, 1) // Mirror
        mColour
    } catch (e: Throwable) {
        null
    }
}

private fun ByteArray?.getBitmapFromFrameNotFlip(width: Int, height: Int): Bitmap? {
    this ?: return null
    return try {
        val argb = this.rgb8ToArgb(width, height)
            ?: return null
        return Bitmap.createBitmap(argb, width, height, Bitmap.Config.RGB_565)
    } catch (e: OutOfMemoryError) {
        null
    } catch (e: Exception) {
        null
    }
}

fun ByteArray.getBitmapFromByte(
    width: Int = RealSenseControl.COLOR_WIDTH,
    height: Int = RealSenseControl.COLOR_HEIGHT
): Bitmap? {
    try {
        val argb = this.rgb8ToArgb(width, height)
            ?: return null
        val bmp = Bitmap.createBitmap(argb, width, height, Bitmap.Config.RGB_565)
        return bmp.flipBitmap()
    } catch (e: java.lang.Exception) {
        return null
    }
}

fun Frame?.getFrameData(): ByteArray? {
    this ?: return null
    val data = ByteArray(this.dataSize)
    this.getData(data)
    return data
}

fun DataCollect.repairCollectData(): DataCollect? {
    return if (!this.isRepaired) {
        val colorData = this.colorData
        if (colorData != null) {

            val bitmap = colorData.getBitmapFromFrameNotFlip(
                RealSenseControl.COLOR_WIDTH,
                RealSenseControl.COLOR_HEIGHT
            )
            val byteArr = BitmapUtils.bitmapToByteArray(bitmap)
            this.frameColorString = Base64.encodeToString(byteArr, Base64.NO_WRAP)

            val stringDepth = Base64.encodeToString(this.depthData, Base64.NO_WRAP)
            this.frameDepthString = stringDepth

            this.colorData = byteArr
            this.isRepaired = true
            this.pointData = this.dataFacePoint?.dataFace?.formatDataFaceHeader() ?: ""
            this
        } else {
            null
        }
    } else {
        this
    }
}

/**
 * format string data face
 */
fun FacePointData.formatDataFaceHeader(): String? {
    val rect = this.faceRect
    val eyeLeft = this.LeftEye
    val eyeRight = this.RightEye
    val mouthLeft = this.Leftmouth
    val mouthRight = this.Rightmouth
    val nose = this.Nose
    val dataFace =
        "${rect.left}a${rect.top}a${rect.right}a${rect.bottom}a${eyeLeft.x}a${eyeLeft.y}a${eyeRight.x}a${eyeRight.y}a${mouthLeft.x}a${mouthLeft.y}a${mouthRight.x}a${mouthRight.y}a${nose.x}a${nose.y}"
    Log.d("dataFaceUtils", dataFace)
    return dataFace
}

/**
 * draw facePointData on bitmap [drawFaceDataToBitmap]
 */
fun FacePointData.drawFaceDataToBitmap(faceBitmap: Bitmap): Bitmap {
    val workingBitmap: Bitmap = Bitmap.createBitmap(faceBitmap)
    val mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true)
    val canvas = Canvas(mutableBitmap)
    val paint = Paint()
    paint.color = Color.RED
    paint.style = Paint.Style.STROKE
    paint.strokeWidth = 5f
    canvas.drawPoint(
        this.LeftEye.x.toFloat(),
        this.LeftEye.y.toFloat(),
        paint
    )
    canvas.drawPoint(
        this.RightEye.x.toFloat(),
        this.RightEye.y.toFloat(),
        paint
    )
    canvas.drawPoint(
        this.Leftmouth.x.toFloat(),
        this.Leftmouth.y.toFloat(),
        paint
    )
    canvas.drawPoint(
        this.Rightmouth.x.toFloat(),
        this.Rightmouth.y.toFloat(),
        paint
    )
    canvas.drawPoint(this.Nose.x.toFloat(), this.Nose.y.toFloat(), paint)

    canvas.drawRect(this.faceRect, paint)
    canvas.save()
    return mutableBitmap
}

fun Rect.drawRectToBitmap(faceBitmap: Bitmap): Bitmap {
    val workingBitmap: Bitmap = Bitmap.createBitmap(faceBitmap)
    val mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true)
    val canvas = Canvas(mutableBitmap)
    val paint = Paint()
    paint.color = Color.RED
    paint.style = Paint.Style.STROKE
    paint.strokeWidth = 5f
    canvas.drawRect(this, paint)
    canvas.save()
    return mutableBitmap
}

/**
 * convert image to nv21
 */
private fun ByteArray.encodeYUV420SP(argb: IntArray, width: Int, height: Int) {
    val frameSize = width * height
    var yIndex = 0
    var uvIndex = frameSize
    var R: Int
    var G: Int
    var B: Int
    var Y: Int
    var U: Int
    var V: Int
    var index = 0
    for (j in 0 until height) {
        for (i in 0 until width) {
            //a = argb[index] and -0x1000000 shr 24
            R = argb[index] and 0xff0000 shr 16
            G = argb[index] and 0xff00 shr 8
            B = argb[index] and 0xff shr 0
            Y = (66 * R + 129 * G + 25 * B + 128 shr 8) + 16
            U = (-38 * R - 74 * G + 112 * B + 128 shr 8) + 128
            V = (112 * R - 94 * G - 18 * B + 128 shr 8) + 128
            this[yIndex++] =
                (if (Y < 0) 0 else if (Y > 255) 255 else Y).toByte()
            if (j % 2 == 0 && index % 2 == 0) {
                this[uvIndex++] =
                    (if (V < 0) 0 else if (V > 255) 255 else V).toByte()
                this[uvIndex++] =
                    (if (U < 0) 0 else if (U > 255) 255 else U).toByte()
            }
            index++
        }
    }
}

fun Bitmap.getNV21(inputWidth: Int, inputHeight: Int): ByteArray {
    val argb = IntArray(inputWidth * inputHeight)
    this.getPixels(argb, 0, inputWidth, 0, 0, inputWidth, inputHeight)
    val yuv = ByteArray(inputWidth * inputHeight * 3 / 2)
    yuv.encodeYUV420SP(argb, inputWidth, inputHeight)
    this.recycle()
    return yuv
}

/**
 * check position face [getCenterPoint], [distancePoint], [getFaceDegreeY]
 */
fun getCenterPoint(point1: Point, point2: Point): Point {
    val disX = abs((point1.x - point2.x)) / 2
    val x = if (point1.x > point2.x) {
        point2.x + disX
    } else {
        point1.x + disX
    }
    val disY = abs((point1.y - point2.y)) / 2
    val y = if (point1.y > point2.y) {
        point2.y + disY
    } else {
        point1.y + disY
    }
    return Point(x, y)
}

fun distancePoint(a: Point, b: Point): Float {
    return sqrt(
        (a.x.toDouble() - b.x.toDouble()).pow(2.0) + (a.y.toDouble() - b.y.toDouble()).pow(
            2.0
        )
    ).toFloat()
}

fun Rect.paddingMTCNN(): Rect {
    val minus = this.width() * 0.12
    val l = this.left + minus
    val t = this.top
    val r = this.right - minus
    val b = this.bottom
    return Rect(l.roundToInt(), t, r.roundToInt(), b)
}

fun Face.getRectRatio(mratio: Double = ratio): Rect {
    val rect = this.boundingBox
    val left = rect.left / mratio
    val top = rect.top / mratio
    val right = rect.right / mratio
    val bottom = rect.bottom / mratio
    return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
}

fun DataGetFacePoint?.checkDegreeFace(): Boolean {
    this ?: return false
    this.dataFace ?: return false
    val y = getNumberToFaceDegreeY(
        this.dataFace.LeftEye, this.dataFace.RightEye,
        this.dataFace.Nose, this.dataFace.Leftmouth, this.dataFace.Rightmouth
    )
    val x = getNumberToFaceDegreeX(
        this.dataFace.LeftEye, this.dataFace.RightEye,
        this.dataFace.Nose, this.dataFace.Leftmouth, this.dataFace.Rightmouth
    )
    return x in -30f..30f && y in -30f..30f
}

fun getNumberToFaceDegreeY(
    pointEyeLeft: Point,
    pointEyeRight: Point,
    pointNose: Point,
    pointMouthLeft: Point,
    pointMouthRight: Point
): Float {
    val pointCenterEye = getCenterPoint(pointEyeLeft, pointEyeRight)
    val pointCenterMouth = getCenterPoint(pointMouthLeft, pointMouthRight)
    val pointCenterY = getCenterPoint(pointCenterEye, pointCenterMouth)
    val rY = distancePoint(pointCenterEye, pointCenterY)
    val disOMY = distancePoint(Point(pointCenterY.x, pointNose.y), pointCenterY)
    val angleDataY = disOMY / rY
    val angleY = acos(angleDataY)
    return if (pointNose.y < pointCenterY.y) (90 - angleY * (180 / PI).toFloat()) else -(90 - angleY * (180 / PI).toFloat())
}

fun getNumberToFaceDegreeX(
    pointEyeLeft: Point,
    pointEyeRight: Point,
    pointNose: Point,
    pointMouthLeft: Point,
    pointMouthRight: Point
): Float {
    val pointCenterEyeMouthLeft = getCenterPoint(pointEyeLeft, pointMouthLeft)
    val pointCenterEyeMouthRight = getCenterPoint(pointEyeRight, pointMouthRight)
    val pointCenterX = getCenterPoint(pointCenterEyeMouthLeft, pointCenterEyeMouthRight)
    val rX = distancePoint(pointCenterEyeMouthLeft, pointCenterX)
    val disOMX = distancePoint(Point(pointNose.x, pointCenterEyeMouthLeft.y), pointCenterX)
    val angleDataX = disOMX / rX
    val angleX = acos(angleDataX)
    return if (pointNose.x > pointCenterX.x) (90 - angleX * (180 / PI).toFloat()) else -(90 - angleX * (180 / PI).toFloat())
}

/**
 * get a face to list face [getLargestFace]
 */
fun Vector<Box>.getLargestFace(): Box? {
    var largest: Box? = null
    return if (this.isNotEmpty()) {
        this.forEach {
            if (largest == null) {
                largest = it
            } else if (largest!!.width() < it.width()) {
                largest = it
            }
        }
        largest
    } else {
        largest
    }
}

fun List<Face>.getLargestMlKitFace(): Face? {
    var largest: Face? = null
    return if (this.isNotEmpty()) {
        this.forEach {
            if (largest == null) {
                largest = it
            } else if (largest!!.boundingBox.width() < it.boundingBox.width()) {
                largest = it
            }
        }
        largest
    } else {
        largest
    }
}

/**
 * check Zone face [checkZoneFace]
 */
fun Box.checkZoneFace(): Boolean {
    return try {
        val left = this.box[0]
        val right = this.box[2]
        val top = this.box[1]
        val bot = this.box[3]
        val x = (left + right) * 0.5f
        val y = (top + bot) * 0.5f
        log.e("zoneFace : $x - $y")
        x in 200f..430f && y in 80f..555f
    } catch (e: java.lang.Exception) {
        false
    }
}

/**
 * check zone face frame hd crop center
 */
fun Rect.checkZoneHDFaceMlKit(): Boolean {
    return try {
        val x = this.exactCenterX()
        val y = this.exactCenterY()
        log.d("checkZoneFaceMlKit : x $x - y $y")
        x in 100f..380f
    } catch (e: java.lang.Exception) {
        false
    }
}

/**
 * check zone face frame hd banuba
 */
fun Rect.checkZoneFaceMlKit(): Boolean {
    return try {
        val x = this.exactCenterX()
        val y = this.exactCenterY()
        log.d("checkZoneFaceMlKit : x $x - y $y")
        x in 18f..100f && y in 25f..108f
    } catch (e: java.lang.Exception) {
        false
    }
}

fun Rect.checkWidthFace(): Boolean {
    log.e("checkWidthFace : ${this.width()}")
    return this.width() < MLKitDetector.MIN_SIZE
}


fun Vector<Box>.getListLargestFace(): ListLargestFaceData? {
    try {
        if (this.isNullOrEmpty()) return null
        if (this.size == 1) {
            return ListLargestFaceData(this[0], null)
        }
        val faceLargest = this.toList().maxByOrNull { it?.width() ?: 0 } ?: return null
        var faceLargest2: Box? = null
        this.forEach {
            if (it.width() >= faceLargest.width() / 1.5 && it.width() != faceLargest.width()) {
                faceLargest2 = it
            }
        }
        return ListLargestFaceData(faceLargest, faceLargest2)
    } catch (e: Exception) {
        return null
    }
}

fun Mat.rotate(isFlip: Boolean = true): Mat {
    val dst = Mat()
    Core.rotate(this, dst, Core.ROTATE_90_COUNTERCLOCKWISE)
    if (isFlip) Core.flip(dst, dst, 1) // Mirror
    this.release()
    return dst
}

fun Mat.matResize(resize: Double = MLKitDetector.RESIZE): Mat? {
    return try {
        val mRatio = resize / this.width().coerceAtLeast(this.height())
        ratio = mRatio
        val downscaledSize = Size(this.width() * ratio, this.height() * ratio)
        val srcResize = Mat(downscaledSize, this.type())
        Imgproc.resize(this, srcResize, downscaledSize)
        srcResize
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

fun Mat.toBitmap(): Bitmap? {
    return try {
        val bitmap = Bitmap.createBitmap(this.cols(), this.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(this, bitmap)
        this.release()
        bitmap
    } catch (e: Exception) {
        null
    }
}

fun Mat.toBitmapNotRelease(): Bitmap? {
    return try {
        val bitmap = Bitmap.createBitmap(this.cols(), this.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(this, bitmap)
        bitmap
    } catch (e: Exception) {
        null
    }
}

fun Mat.toBanubaBitmap(): Bitmap? {
    return try {
        val bitmap = Bitmap.createBitmap(this.cols(), this.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(this, bitmap)
        this.release()
        val w = bitmap.width
        val h = w * 1208 / 840
         /*val h = (w * 16 / 9.9).toInt()*/
        val hBitmap = Bitmap.createBitmap(w,h, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(hBitmap)
        canvas.drawColor(Color.WHITE)
        canvas.drawBitmap(bitmap,0f,0f, null)
        hBitmap
    } catch (e: Exception) {
        null
    }
}

fun Mat.cropRect(rect: Rect): Mat {
    val w = this.cols()
    val h = this.rows()
    val rectCrop = rect.getRectCrop(w, h).toOpenCVRoi()
    val cropped = Mat(this, rectCrop)
    this.release()
    return cropped
}


fun Box.getRectRatio(): Rect {
    val rect = this.transform2Rect()
    val left = rect.left / ratio
    val top = rect.top / ratio
    val right = rect.right / ratio
    val bottom = rect.bottom / ratio
    return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
}

fun PointF.getRatio(): PointF {
    val x = this.x / ratio.toFloat()
    val y = this.y / ratio.toFloat()
    return PointF(x, y)
}

fun Bitmap.cropRect(rect: Rect): Bitmap {
    val cropRect = rect.getRectCrop(this.width, this.height)
    val cropped =
        Bitmap.createBitmap(this, cropRect.left, cropRect.top, cropRect.width(), cropRect.height())
    this.recycle()
    return cropped
}

fun Rect.getRectCrop(w: Int, h: Int): Rect {
    val top = if (top < 0) 0 else top
    val left = if (left < 0) 0 else left
    val right = if (right > w) w else right
    val bottom = if (bottom > h) h else bottom
    return Rect(left, top, right, bottom)
}

fun Rect.toOpenCVRoi(): org.opencv.core.Rect {
    return org.opencv.core.Rect(left, top, this.width(), this.height())
}

fun checkBrightness(src: Mat?): Double {
    src ?: return 0.0
    val w = src.cols()
    val h = src.rows()
    val minus = (w * 0.28).roundToInt()
    val l = minus
    val t = minus
    val r = w - minus
    val b = h - minus
    val rectcrop = Rect(l, t, r, b)
    val cropSrc = src.cropRect(rectcrop)
    val gray = Mat()
    Imgproc.cvtColor(cropSrc, gray, Imgproc.COLOR_BGR2GRAY)
    val value = Core.mean(gray).`val`[0]
    src.release()
    gray.release()
    cropSrc.release()
    return value
}

fun Rect.cropPortrait(bitmap: Bitmap): Bitmap? {

    val plusH = height() * 0.5
    val minusWH = (height() - width()) / 2

    val plusW = (width() + minusWH) * 0.5
    val height = height() + plusH.roundToInt()
    val width = width() + plusW.roundToInt()

    var top = top - (plusH / 2).roundToInt()
    var left = left - (plusW / 2).roundToInt()
    if (top < 0) top = 0
    if (left < 0) left = 0

    val newRect = Rect(left, top, left + width, top + height)
    val rectCrop = newRect.getRectCrop(bitmap)
    val copiedBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
    return try {
        val crop = Bitmap.createBitmap(
            copiedBitmap,
            rectCrop.left,
            rectCrop.top,
            rectCrop.width(),
            rectCrop.height()
        )
        crop
    } catch (t: Throwable) {
        null
    }
}

fun Rect.getRectCrop(bitmap: Bitmap): Rect {
    val top = if (top < 0) 0 else top
    val left = if (left < 0) 0 else left
    val right = if (right > bitmap.width) bitmap.width else right
    val bottom = if (bottom > bitmap.height) bitmap.height else bottom
    return Rect(left, top, right, bottom)
}

fun Face.checkFaceOke(): Boolean {
    val x = this.headEulerAngleX
    val y = this.headEulerAngleY
    val z = this.headEulerAngleZ
    val boxWidth = this.boundingBox.width()
    return x in MLKitDetector.rangeX && y in MLKitDetector.rangeY
}

fun getRawRectTracking(width: Int = MLKitDetector.width, height: Int = MLKitDetector.height): Rect {
    val l = (width * 0.22).roundToInt()
    val t = (height * 0.38).roundToInt()
    val r = width - l
    val b = t + (height * 0.095).roundToInt()
    return Rect(l, t, r, b)
}