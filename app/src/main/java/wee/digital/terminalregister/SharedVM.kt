package wee.digital.terminalregister

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okio.Timeout
import wee.digital.terminalregister.message.MessageArg
import wee.digital.terminalregister.repository.model.SocketBody
import wee.digital.terminalregister.ui.base.BaseVM
import wee.digital.terminalregister.ui.base.EventLiveData
import java.util.concurrent.atomic.AtomicInteger

class SharedVM : BaseVM() {
    val dataSocketLiveData = MutableLiveData<SocketBody>()
    val message = MutableLiveData<MessageArg?>()
    val dismissDialog = EventLiveData<Boolean>()

}