package wee.digital.terminalregister

import androidx.viewbinding.ViewBinding
import wee.digital.citigym_register.ui.base.BaseDialog
import wee.digital.library.extension.Inflate
import wee.digital.terminalregister.ui.base.activityVM

abstract class MainDialog<VB : ViewBinding>(val inf: Inflate<VB>) : BaseDialog<VB>(inf) {

    val sharedVM by lazy { activityVM(SharedVM::class) }

}