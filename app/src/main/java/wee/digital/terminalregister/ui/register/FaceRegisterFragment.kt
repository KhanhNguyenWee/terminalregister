package wee.digital.terminalregister.ui.register

import android.animation.ValueAnimator
import android.graphics.Bitmap
import android.util.Base64
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.lifecycleScope
import com.google.mlkit.vision.face.Face
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.opencv.core.Mat
import wee.digital.library.extension.*
import wee.digital.library.util.Shell
import wee.digital.terminalregister.BuildConfig
import wee.digital.terminalregister.R
import wee.digital.terminalregister.app.App
import wee.digital.terminalregister.camera.face.MyDetection
import wee.digital.terminalregister.camera.face.RealSenseControl
import wee.digital.terminalregister.databinding.FragmentFaceEnrollBinding
import wee.digital.terminalregister.message.MessageArg
import wee.digital.terminalregister.repository.DataCollect
import wee.digital.terminalregister.repository.DataGetFacePoint
import wee.digital.terminalregister.repository.model.FaceRegisterReq
import wee.digital.terminalregister.repository.model.FaceRegisterResponse
import wee.digital.terminalregister.shared.Code
import wee.digital.terminalregister.shared.Shared
import wee.digital.terminalregister.ui.base.MainFragment
import wee.digital.terminalregister.ui.base.activityVM
import wee.digital.terminalregister.ui.base.viewModel
import wee.digital.terminalregister.ui.main.Main
import wee.digital.terminalregister.ui.result.ResultVM
import wee.digital.terminalregister.utils.viewTransition

class FaceRegisterFragment: MainFragment<FragmentFaceEnrollBinding>(FragmentFaceEnrollBinding::inflate),
    MyDetection.DetectionCallBack, RealSenseControl.Listener {
    private var detection: MyDetection = MyDetection()

    private var jobDetection: Job? = null

    private var jobUnApply: Job? = null

    private var jobApply: Job? = null

    private var isDetectFace = false

    private var bindFrame = true

    private var isEffect = true

    private val faceLoading get() = bd.faceLoadingInclude

    private val faceVM by lazy { viewModel(FaceVM::class) }

    private val resultVM by lazy { activityVM(ResultVM::class) }


    override fun onViewCreated() {
        App.realSenseControl?.hasFace()
//        Shell.removeWatermarkBanuba(BuildConfig.APPLICATION_ID)
        lifecycleScope.launch {
            wee.digital.terminalregister.app.app.banubaImp?.start(bd.faceSurfaceView)
            isEffect = wee.digital.terminalregister.app.app.banubaImp?.hasEffect ?: false
            detection.listener = this@FaceRegisterFragment
            isDetectFace = true
        }
    }

    override fun onLiveDataObserve() {
        Shared.imageLiveData.observe { bindFrameBanuba(it) }

        faceVM.enrollSuccessEvent.observe {
            handleSuccess(it)
        }

        faceVM.enrollFailEvent.observe {
            handleFail(it)
        }
    }

    private fun handleFail(it: Int) {
        val mess = MessageArg.fromCode(it).apply {
            buttonClose = string(R.string.message_action_accept)
            onClose = { navigate(Main.adv) { setLaunchSingleTop() } }
        }
        sharedVM.message.postValue(mess)
    }

    private fun handleSuccess(it: FaceRegisterResponse?) {
        it?: return
        when (it.code) {
            0 -> {
                faceLoading.faceLoadingRootLoading.hide()
                faceLoading.faceLoadingViewDone.show()
                addClickListener(faceLoading.rootButtonDone)
            }
            Code.BIO_ALREADY_EXIST -> {
                handleFail(it.code)
            }
        } 
        
    }

    override fun onCameraData(colorBitmap: Mat?, depthBitmap: Mat?, dataCollect: DataCollect?) {
        colorBitmap ?: return
        depthBitmap ?: return
        if (isDetectFace) {
            detection.mlKitDetectFrame(colorBitmap, depthBitmap, dataCollect)
        }
    }

    /**
     * listener detection face
     */
    override fun faceNotFrame() {
        lifecycleScope.launch(Dispatchers.Main) {
            actionUnApplyEffect()
            bd.faceLabelStatus.show()
        }
    }

    override fun faceOnFrame() {
        lifecycleScope.launch(Dispatchers.Main) {
            actionApplyEffect()
            bd.faceLabelStatus.hide()
            App.realSenseControl?.hasFace()
        }
    }

    override fun faceEligible(face: Face, dataFace: DataGetFacePoint, dataCollect: DataCollect?) {
        stopFrameCamera()
        lifecycleScope.launch(Dispatchers.Main) {
            bd.faceSurfaceView.gone()
            bd.faceBackground.show()
            val bitmap = dataFace.face.decodeToBitmap()
            bd.faceBackground.closeCamera(bitmap) {
                animationScaleFace()
                faceResult(dataFace.face)
            }
        }
    }

    private fun stopFrameCamera() {
        bindFrame = false
        isDetectFace = false
        cancelJob()
    }

    private fun cancelJob() {
        jobDetection?.cancel()
        jobUnApply?.cancel()
        jobApply?.cancel()
    }

    /**
     * banuba
     */
    private fun bindFrameBanuba(colorFrame: Bitmap?) {
        colorFrame ?: return
        synchronized(bindFrame) {
            if (bindFrame) {
                wee.digital.terminalregister.app.app.banubaImp?.pushBitmap(colorFrame)
            }
        }
    }

    private fun actionApplyEffect() {
        if (isEffect) return
        isEffect = true
        isDetectFace = false
        jobApply?.cancel()
        jobApply = lifecycleScope.launch(Dispatchers.Main) {
            wee.digital.terminalregister.app.app.banubaImp?.applyEffect {}
            delay(1000)
            isDetectFace = true
        }
    }

    private fun actionUnApplyEffect() {
        if (!isEffect) return
        isEffect = false
        isDetectFace = false
        jobUnApply?.cancel()
        jobUnApply = lifecycleScope.launch(Dispatchers.Main) {
            wee.digital.terminalregister.app.app.banubaImp?.unApplyEffect {}
            delay(1000)
            isDetectFace = true
        }
    }

    /**
     * animation view
     */
    private fun animationScaleFace() {
        viewTransition.beginTransition(bd.faceEnrollView) {
            this.clear(bd.faceBackground.id, ConstraintSet.TOP)
            this.clear(bd.faceBackground.id, ConstraintSet.BOTTOM)
            connect(bd.faceBackground.id, ConstraintSet.TOP, bd.faceGuideline.id, ConstraintSet.TOP)
        }
        val valueScale = ValueAnimator.ofFloat(1f, 0.5f).apply {
            interpolator = LinearInterpolator()
            duration = 300
            addUpdateListener {
                val value = it.animatedValue as Float
                bd.faceBackground.scaleX = value
                bd.faceBackground.scaleY = value
            }
        }
        valueScale.start()
    }

    private fun faceResult(frame: ByteArray?) {
        animLoading()
        // api call
        val faceReq = FaceRegisterReq().apply {
            this.photo = Base64.encodeToString(frame, Base64.NO_WRAP)
            this.cif = Shared.getUUIDRandom()
        }
        faceVM.enrollNewUser(faceReq)
    }

    private fun animLoading() {
        faceLoading.faceLoadingGifLoad.load(R.mipmap.img_progress_small)
        val bannerId = bd.faceRootBanner.id
        val shadowBannerId = bd.faceShadowBanner.id
        viewTransition.beginTransition(bd.faceEnrollView) {
            this.clear(bannerId, ConstraintSet.BOTTOM)
            connect(bannerId, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
            connect(bannerId, ConstraintSet.TOP, bd.faceGuideline4.id, ConstraintSet.TOP)

            this.clear(shadowBannerId, ConstraintSet.TOP)
            connect(shadowBannerId, ConstraintSet.TOP, bd.faceGuideline3.id, ConstraintSet.TOP)
        }
    }

    override fun onViewClick(v: View?) {
        when (v) {
            faceLoading.rootButtonDone -> {
                navigate(Main.adv) {
                    setLaunchSingleTop(true)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        App.realSenseControl?.listener = this
    }

    override fun onPause() {
        super.onPause()
        App.realSenseControl?.listener = null
    }

}