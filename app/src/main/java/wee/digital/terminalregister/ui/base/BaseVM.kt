package wee.digital.terminalregister.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import wee.digital.library.util.Logger
import wee.digital.terminalregister.repository.model.ApiResponse

abstract class BaseVM : ViewModel() {

    val log by lazy {
        Logger(this.javaClass.simpleName)
    }

    open fun Flow<ApiResponse>.checkResponse(
        onSuccess: (ApiResponse) -> Unit,
        onFailed: (ApiResponse) -> Unit = {}
    ) {
        this@checkResponse
            .onEach {
                if (it.code == 0) {
                    onSuccess(it)
                } else {
                    onFailed(it)
                }
            }
            .launchIn(viewModelScope)
    }

}