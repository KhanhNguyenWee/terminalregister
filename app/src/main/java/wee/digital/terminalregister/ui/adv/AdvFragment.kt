package wee.digital.terminalregister.ui.adv

import android.view.View
import wee.digital.terminalregister.app.App
import wee.digital.terminalregister.databinding.FragmentAdvBinding
import wee.digital.terminalregister.ui.base.MainFragment
import wee.digital.terminalregister.ui.base.viewModel
import wee.digital.terminalregister.ui.main.Main
import wee.digital.terminalregister.utils.ex.pauseCamera
import wee.digital.terminalregister.utils.ex.startCamera

class AdvFragment : MainFragment<FragmentAdvBinding>(FragmentAdvBinding::inflate) {

    private val advAdapter = AdvAdapter()

    private val advVM: AdvVM by lazy { viewModel(AdvVM::class) }
    override fun onViewCreated() {
        addClickListener(bd.viewCheckIn,bd.advViewPager)
        configAdv()
        pauseCamera()
        bd.advViewPager.isUserInputEnabled = false
    }

    private fun configAdv() {
        advVM.fetchAdvList()
        advVM.countdownToNextSlide()
    }

    override fun onLiveDataObserve() {
        advVM.imagesLiveData.observe {
            advAdapter.set(it)
            advAdapter.bindToViewPager(bd.advViewPager)
        }
        advVM.pageLiveData.observe {
            var i = bd.advViewPager.currentItem
            if (i == advAdapter.currentList.size - 1) {
                bd.advViewPager.setCurrentItem(0, true)
            } else {
                bd.advViewPager.setCurrentItem(++i, true)
            }
        }
        advAdapter.onPageChanged.observe {
            val i = bd.advViewPager.currentItem
            if (advAdapter.get(i)?.isImage == true) {
                advVM.countdownToNextSlide()
            }
        }
    }

    override fun onViewClick(v: View?) {
        when (v) {
            bd.viewCheckIn, bd.advViewPager -> {
                startCamera()
                navigate(Main.faceRegister)
            }
        }
    }
}