package wee.digital.terminalregister.ui.base

import androidx.viewbinding.ViewBinding
import wee.digital.citigym_register.ui.base.BaseFragment
import wee.digital.library.extension.Inflate
import wee.digital.terminalregister.SharedVM

abstract class MainFragment<VB : ViewBinding>(val inf: Inflate<VB>) : BaseFragment<VB>(inf) {

    val sharedVM by lazy { activityVM(SharedVM::class) }

}