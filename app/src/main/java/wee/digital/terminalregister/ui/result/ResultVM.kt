package wee.digital.terminalregister.ui.result

import androidx.lifecycle.MutableLiveData
import wee.digital.terminalregister.ui.base.BaseVM

class ResultVM : BaseVM(){
    val resultArgLiveData = MutableLiveData<ResultArg>()

}