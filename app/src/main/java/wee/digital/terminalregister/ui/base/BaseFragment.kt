package wee.digital.citigym_register.ui.base

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import wee.digital.library.extension.Inflate
import wee.digital.library.util.Logger
import wee.digital.terminalregister.ui.base.BaseActivity
import wee.digital.terminalregister.ui.base.BaseView
import wee.digital.terminalregister.ui.base.NonNullLiveData

abstract class BaseFragment<VB : ViewBinding>(val inflate: Inflate<VB>) : Fragment(), BaseView {

    lateinit var bd: VB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bd = inflate.invoke(inflater, container, false)
        return bd.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        log.d("onViewCreated")
//        this.activity().mKeyboard?.setDisableSoftKeyboard()
        onViewCreated()
        onLiveDataObserve()
    }

    override fun onPause() {
        super.onPause()
        view?.clearAnimation()
//        this.activity().mKeyboard?.dismissPopupKeyboard()
    }

    /**
     * [BaseFragment] required implements
     */
    abstract fun onViewCreated()

    abstract fun onLiveDataObserve()

    /**
     * [BaseView] implement
     */
    final override val nav get() = findNavController()

    final override val log by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        Logger("BaseView.${this::class.java.simpleName}")
    }

    /**
     * [BaseFragment] properties
     */
    fun <T> LiveData<T>.observe(block: (T) -> Unit) {
        observe(viewLifecycleOwner, Observer(block))
    }

    fun <T> NonNullLiveData<T?>.observe(block: (T) -> Unit) {
        observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            block(it)
        })
    }

    fun startClear(cls: Class<*>) {
        activity().run {
            val intent = Intent(this, cls)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            this.startActivity(intent)
            this.finish()
        }

    }

    open fun activity(): BaseActivity<*> {
        if (activity !is BaseActivity<*>) throw ClassCastException("BaseFragment must be owned in BaseActivity")
        return activity as BaseActivity<*>
    }

    init {
        log
    }

}