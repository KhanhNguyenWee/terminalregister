package wee.digital.terminalregister.ui.result

import wee.digital.terminalregister.databinding.FragmentResultBinding
import wee.digital.terminalregister.ui.base.MainFragment

class ResultFragment: MainFragment<FragmentResultBinding>(FragmentResultBinding::inflate) {
    override fun onViewCreated() {

    }

    override fun onLiveDataObserve() {
    }
}