package wee.digital.terminalregister.ui.main

import wee.digital.terminalregister.MainGraphDirections


object Main {
    val adv = MainGraphDirections.actionGlobalAdvFragment()
    val faceRegister = MainGraphDirections.actionGlobalFaceRegisterFragment()
    val result = MainGraphDirections.actionGlobalResultFragment()
    val message = MainGraphDirections.actionGlobalMessageFragment()
}