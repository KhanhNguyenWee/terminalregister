package wee.digital.citigym_register.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import wee.digital.library.extension.Inflate
import wee.digital.library.extension.hideSystemUI
import wee.digital.library.util.Logger
import wee.digital.terminalregister.R
import wee.digital.terminalregister.ui.base.BaseView

abstract class BaseDialog <VB : ViewBinding>(val inflate : Inflate<VB>) : DialogFragment(),
    BaseView {

//    var mKeyboard: PopupKeyboard? = null

    lateinit var bd: VB

    /**
     * [DialogFragment] override
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, style())
        dialog?.window?.attributes?.windowAnimations = animStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dialog?.window?.attributes?.windowAnimations = animEnd()
        log.d("onViewDestroy")
//        mKeyboard?.dismissPopupKeyboard()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        mKeyboard = PopupKeyboard(this.requireActivity())
        bd = inflate.invoke(inflater, container, false)
        return bd.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        log.d("onViewCreated")
        onViewCreated()
        onLiveDataObserve()
//        mKeyboard?.setDisableSoftKeyboard()
    }

    override fun onStart() {
        super.onStart()
        when (style()) {
            R.style.App_Dialog_FullScreen,
            R.style.App_Dialog_FullScreen_Transparent -> dialog?.window?.apply {
                setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        log.d("onResume")
//        mKeyboard?.dismissPopupKeyboard()
        hideSystemUI()
    }

    override fun onPause() {
        super.onPause()
//        mKeyboard?.dismissPopupKeyboard()
        log.d("onPause")
    }

    /**
     * [BaseDialog] Required implements
     */
    abstract fun onViewCreated()

    abstract fun onLiveDataObserve()

    protected open fun style(): Int {
        return R.style.App_Dialog_FullScreen
    }

    open fun animStart(): Int {
        return R.style.App_Dialog_FullScreen
    }

    open fun animEnd(): Int {
        return R.style.App_Dialog_FullScreen
    }

    /**
     * [BaseView] implement
     */
    final override val nav get() = findNavController()

    final override val log by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        Logger("BaseView.${this::class.java.simpleName}")
    }

    /**
     * [BaseDialog] properties
     */
    fun <T> LiveData<T>.observe(block: (T) -> Unit) {
        observe(viewLifecycleOwner, Observer(block))
    }

    init {
        log
    }

}