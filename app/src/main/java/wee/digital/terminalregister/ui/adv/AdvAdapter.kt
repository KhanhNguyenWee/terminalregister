package wee.digital.terminalregister.ui.adv

import androidx.viewbinding.ViewBinding
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import wee.digital.library.adapter.BaseBindRecyclerAdapter
import wee.digital.library.adapter.ItemInflating
import wee.digital.library.extension.load
import wee.digital.terminalregister.databinding.ItemAdvImageBinding
import wee.digital.terminalregister.repository.model.AdvItem
import wee.digital.terminalregister.ui.base.EventLiveData

class AdvAdapter : BaseBindRecyclerAdapter<AdvItem>() {

    /**
     * Current of viewpager position adapt this adapter
     */
    var currentPosition: Int = -1

    var onPageChanged = EventLiveData<Boolean>()

    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun get(position: Int): AdvItem? {
        if (currentList.isEmpty()) return null
        val realPosition = position % currentList.size
        if (realPosition !in 0..lastIndex) return null
        return currentList[realPosition]
    }

    override fun itemInflating(item: AdvItem, position: Int): ItemInflating {
        return ItemAdvImageBinding::inflate
    }

    override fun ViewBinding.onBindItem(item: AdvItem, position: Int) {
        currentPosition = position
        when (this) {
            is ItemAdvImageBinding -> {
                advImageView.load(item.imageRes!!)
            }
        }
    }

    /**
     * ViewHolder util
     */
    private var viewPager: ViewPager2? = null

    fun bindToViewPager(viewPager: ViewPager2) {
        this.viewPager = viewPager
        viewPager.adapter = this
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                when (state) {
                    ViewPager.SCROLL_STATE_DRAGGING -> {
                    }
                    ViewPager.SCROLL_STATE_IDLE -> {
                        onPageChanged.postValue(true)
                        if (currentPosition != -1) {
                            //get(currentPosition)?.videoController?.stopVideo()
                        }
                        currentPosition = viewPager.currentItem
                        //get(currentPosition)?.videoController?.playVideo()
                    }
                }
            }
        })
    }

}