package wee.digital.terminalregister.ui.result

import wee.digital.library.extension.string
import wee.digital.terminalregister.R

class ResultArg {

    var imageIcon: String = ""

    var title: String = ""

    var message: String = ""

    companion object {
        val success = ResultArg().apply {
            imageIcon = "drawable/ic_success_green"
            title = string(R.string.success_title)
            message = ""
        }

        val exist = ResultArg().apply {
            imageIcon = "drawable/ic_success_blue"
            title = string(R.string.already_check_in_title)
            message = string(R.string.already_check_in_text)
        }

        val fail = ResultArg().apply {
            imageIcon = "drawable/ic_fail"
            title = string(R.string.fail_title)
            message = string(R.string.fail_text)
        }
    }

}