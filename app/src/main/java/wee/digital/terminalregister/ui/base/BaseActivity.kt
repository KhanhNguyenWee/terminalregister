package wee.digital.terminalregister.ui.base

import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.viewbinding.ViewBinding
import wee.digital.library.extension.hideSystemUI
import wee.digital.library.util.Logger

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity(), BaseView {

    lateinit var bd : VB


    /**
     * [AppCompatActivity] override
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        log.d("onCreate")
        bd = viewBinding()
        setContentView(bd.root)
        onViewCreated()
        onLiveDataObserve()
    }

    /**
     * [BaseActivity] abstract implements
     */
    abstract fun viewBinding() : VB

    abstract fun onViewCreated()

    abstract fun onLiveDataObserve()

    /**
     * [BaseView] implement
     */
    final override val nav get() = findNavController(navigationHostId())

    final override val log by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        Logger("BaseView.${this::class.java.simpleName}")
    }

    /**
     * implement lifeCycle
     */
    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    override fun onPause() {
        super.onPause()
        hideSystemUI()
    }

    /**
     * [BaseActivity] properties
     */
    open fun navigationHostId(): Int {
        return 0
    }

    fun <T> LiveData<T>.observe(block: (T) -> Unit) {
        observe(this@BaseActivity, Observer(block))
    }

    fun <T> NonNullLiveData<T?>.observe(block: (T) -> Unit) {
        observe(this@BaseActivity, Observer {
            it ?: return@Observer
            block(it)
        })
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            hideSystemUI()
        }
        return super.dispatchTouchEvent(event)
    }

    fun startClear(cls: Class<*>) {
        this.run {
            val intent = Intent(this, cls)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            this.startActivity(intent)
            this.finish()
        }
    }

}