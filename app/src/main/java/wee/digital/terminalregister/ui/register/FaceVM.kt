package wee.digital.terminalregister.ui.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import wee.digital.library.extension.parse
import wee.digital.terminalregister.repository.FaceRepository
import wee.digital.terminalregister.repository.model.FaceRegisterReq
import wee.digital.terminalregister.repository.model.FaceRegisterResponse
import wee.digital.terminalregister.ui.base.BaseVM
import wee.digital.terminalregister.ui.base.EventLiveData

class FaceVM: BaseVM()  {
    var enrollSuccessEvent = EventLiveData<FaceRegisterResponse>()
    var enrollFailEvent = EventLiveData<Int>()


    fun enrollNewUser(data: FaceRegisterReq) {
        viewModelScope.launch(Dispatchers.IO) {
            FaceRepository.registerFace(data).checkResponse(
                onFailed = {
                    Log.d("enrollNewUser fail","$it - ${it.message}")
                    enrollFailEvent.postValue(it.code)
                },

                onSuccess = {
                    val resp = it.data.toString().parse(FaceRegisterResponse::class.java) ?: FaceRegisterResponse()
                    Log.d("enrollNewUser fail","${it.data}")
                    enrollSuccessEvent.postValue(resp)
                    
                }
            )
        }

    }
}