package wee.digital.terminalregister.ui.adv

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import wee.digital.terminalregister.R
import wee.digital.terminalregister.repository.model.AdvItem
import wee.digital.terminalregister.ui.base.BaseVM
import wee.digital.terminalregister.ui.base.EventLiveData


class AdvVM : BaseVM() {

    private val listAdv = listOf(
        AdvItem(imageRes = R.mipmap.napas_adv_1),
        AdvItem(imageRes = R.mipmap.napas_adv_2)
    )

    val imagesLiveData = EventLiveData<List<AdvItem>?>()

    val pageLiveData = EventLiveData<Boolean>()

    fun fetchAdvList() {
        imagesLiveData.postValue(listAdv)
    }

    fun countdownToNextSlide() {
        viewModelScope.launch {
            delay(5000)
            pageLiveData.postValue(true)
        }
    }
}