package wee.digital.terminalregister.widgets

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.animation.LinearInterpolator
import wee.digital.library.widget.AppBindCustomView
import wee.digital.terminalregister.R
import wee.digital.terminalregister.databinding.ViewBackgroundPaymentBinding

class BackgroundPayment(context: Context, attrs: AttributeSet?) :
    AppBindCustomView<ViewBackgroundPaymentBinding>(
        context,
        attrs,
        ViewBackgroundPaymentBinding::inflate
    ) {

    override fun onViewInit(context: Context, types: TypedArray) {
        bd.viewBackgroundFace.alpha = 0f
        bd.viewBackgroundFaceCustom.alpha = 0f
    }

    // check position face
    fun closeCamera(face: Bitmap?, block: () -> Unit) {
        bd.viewBackgroundFaceCustom.setImageBitmap(face)
        bd.viewBackgroundFace.animate().alphaBy(0f).alpha(1f).setDuration(500).start()

        val valueShowLogo = ValueAnimator.ofFloat(1f, 0.6f).apply {
            interpolator = LinearInterpolator()
            duration = 300
            addUpdateListener {
                val value = it.animatedValue as Float
                bd.viewParentBgImage.scaleX = value
                bd.viewParentBgImage.scaleY = value
                if (value <= 0.6f) {
                    block()
                    bd.viewBackgroundFaceCustom.animate().alphaBy(0f).alpha(1f).setDuration(300)
                        .start()
                }
            }
        }
        valueShowLogo.start()
    }

    fun animAlphaSuccess() {
        ValueAnimator.ofFloat(1f, 0f).apply {
            interpolator = LinearInterpolator()
            duration = 300
            addUpdateListener {
                val alpha = it.animatedValue as Float
                bd.viewBackgroundFaceCustom.alpha = alpha
                if (alpha >= 1f) {
                    bd.viewBgLogoGifTick.playAnimation()
                }
            }
        }.start()
    }

    fun animationLogoSuccess() {
        var isChangeBg = false
        val valueRotation = ValueAnimator.ofFloat(0f, 180f).apply {
            interpolator = LinearInterpolator()
            duration = 300
            addUpdateListener {
                val value = it.animatedValue as Float
                bd.viewParentBgImage?.rotationY = value
                if (value >= 90f && !isChangeBg) {
                    isChangeBg = true
                    bd.viewBackgroundFace?.setImageResource(R.color.colorGreen)
                    bd.viewBackgroundFaceCustom.setImageResource(R.color.colorGreen)
                    bd.viewBgLogoGifTick?.playAnimation()
                }
            }
        }
        valueRotation.start()
    }

}