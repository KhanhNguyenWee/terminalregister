# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keep class androidx.security.* { *; }
-keepclassmembers class androidx.security.* { *; }

-keep class com.intel** { *; }
-keepclassmembers class com.intel** { *; }

-keep class wee.digital.ft.camera.* { *; }
-keepclassmembers class  wee.digital.ft.camera.* { *; }

-keep class wee.digital.ft.camera.face.* { *; }
-keepclassmembers class  wee.digital.ft.camera.face.* { *; }

-keep class wee.digital.ft.camera.qr.* { *; }
-keepclassmembers class  wee.digital.ft.camera.qr.* { *; }

-keep class crypto.* { *; }
-keepclassmembers class crypto.* { *; }

-keep class com.bumptech.* { *; }
-keepclassmembers class com.bumptech.* { *; }

-keep class go.Seq.* { *; }
-keepclassmembers class  go.Seq.* { *; }

-keep class com.epson.* { *; }
-keepclassmembers class  com.epson.* { *; }

-keep class wee.digital.ft.repository.model.*{ *; }
-keepclassmembers class wee.digital.ft.repository.model.*{ *; }

-keep class com.HEROFUN.* { *; }
-keepclassmembers class com.HEROFUN.* { *; }

-keep class android**
-keepclassmembers class android** {*;}

-keep class androidx**
-keepclassmembers class androidx** {*;}

-keep class kotlinx**
-keepclassmembers class kotlinx** {*;}

-keep class com.squareup**
-keepclassmembers class com.squareup** {*;}

-keep class com.bumptech**
-keepclassmembers class com.bumptech** {*;}